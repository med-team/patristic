Source: patristic
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 10),
               default-jdk,
               maven-debian-helper,
               libfreehep-graphics2d-java,
               libfreehep-export-java
Standards-Version: 4.1.2
Vcs-Browser: https://salsa.debian.org/med-team/patristic
Vcs-Git: https://salsa.debian.org/med-team/patristic.git
Homepage: http://www.bioinformatics.org/patristic/

Package: patristic
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Calculate patristic distances and comparing the components of genetic change
 Patristic overcomes some logistic barriers to analysing signals in
 sequences. In additional to calculating patristic distances, it provides
 plots for any combination of matrices, calculates commonly used
 statistics, allows data such as isolation dates to be entered and
 reorders matrices with matching species or gene labels. It will be used
 to analyse rates of mutation and substitutional saturation and the
 evolution of viruses.
