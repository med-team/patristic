/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author  Fourment Mathieu
 */
package Patristic;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.JScrollPane;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.JOptionPane;
import java.awt.*;
import java.awt.event.*;
   import java.io.*;

public class Polytomy extends JFrame {
    private boolean DEBUG = false;
    MyTableModel myModel;
    TextField text=new TextField(50);

    public Polytomy(Matrix list) {
        super("Polytomy tool");

        myModel = new MyTableModel(list);
        JTable table = new JTable(myModel);
        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        Panel top = new Panel();
        Button button = new Button("Get Tree");
        top.add(button,BorderLayout.CENTER);
        button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                save();
            }
        });
        
        top.add(text);
        getContentPane().add(top, BorderLayout.NORTH);

        //Create the scroll pane and add the table to it. 
        JScrollPane scrollPane = new JScrollPane(table);

        //Add the scroll pane to this window.
        getContentPane().add(scrollPane, BorderLayout.CENTER);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    
    private void save(){
        String result="(";
        String poly1="(";
        String poly2="";
        
        for(int i=0;i<myModel.getRowCount();i++){
            if(myModel.getValueAt(i,0).toString().compareTo("true")==0){
                poly1+=String.valueOf(i+1)+",";
            }
            else poly2+=String.valueOf(i+1)+",";
        }
        poly1=poly1.substring(0,poly1.length()-1);
        poly1+=")";
        result=result+poly2+poly1+")";
        text.setText(result);
//        Saver save=new Saver(result,new File("."));
    }

    class MyTableModel extends AbstractTableModel {
        final String[] columnNames = {"","taxon"};
        final Object[][] data;
        
        public MyTableModel(Matrix list){
            super();
            data = new Object[list.getNames().length][2];
            for(int i=0;i<list.getNames().length;i++){
                data[i][0]=new Boolean(false);
                data[i][1]=list.getNames()[i];
             }
        }

        public int getColumnCount() {
            return columnNames.length;
        }
        
        public int getRowCount() {
            return data.length;
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        /*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),
         * rather than a check box.
         */
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        /*
         * Don't need to implement this method unless your table's
         * editable.
         */
        public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (col > 0) { 
                return false;
            } else {
                return true;
            }
        }

        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int col) {
            if (DEBUG) {
                System.out.println("Setting value at " + row + "," + col
                                   + " to " + value
                                   + " (an instance of " 
                                   + value.getClass() + ")");
            }

            if (data[0][col] instanceof Integer                        
                    && !(value instanceof Integer)) {                  
                //With JFC/Swing 1.1 and JDK 1.2, we need to create    
                //an Integer from the value; otherwise, the column     
                //switches to contain Strings.  Starting with v 1.3,   
                //the table automatically converts value to an Integer,
                //so you only need the code in the 'else' part of this 
                //'if' block.                                          
                //XXX: See TableEditDemo.java for a better solution!!!
                try {
                    data[row][col] = new Integer(value.toString());
                    fireTableCellUpdated(row, col);
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(Polytomy.this,
                        "The \"" + getColumnName(col)
                        + "\" column accepts only integer values.");
                }
            } else {
                data[row][col] = value;
                fireTableCellUpdated(row, col);
            }

            if (DEBUG) {
                System.out.println("New value of data:");
                printDebugData();
            }
        }

        private void printDebugData() {
            int numRows = getRowCount();
            int numCols = getColumnCount();

            for (int i=0; i < numRows; i++) {
                System.out.print("    row " + i + ":");
                for (int j=0; j < numCols; j++) {
                    System.out.print("  " + data[i][j]);
                }
                System.out.println();
            }
            System.out.println("--------------------------");
        }
    }

    public static void main(String[] args) {
//        Polytomy frame = new Polytomy();
//        frame.pack();
//        frame.setVisible(true);
    }
}
