/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 *
 * @author  formmat
 */
package Patristic;

public class MathStat {


    // correlation for distance plot
    public static double correlation(Matrix matrix1, Matrix matrix2, double mean1, double mean2){
        double result=0.0;
        double variance1=0.0;
        double variance2=0.0;
        double covariance=0.0;

        for(int i=0;i<matrix1.getNames().length;i++){
            for(int j=i+1;j<matrix1.getNames().length;j++){
                double value1=matrix1.getMatrix()[i][j] - mean1;
                double value2=matrix2.getMatrix()[i][j] - mean2;
                variance1=variance1 + (value1*value1);
                variance2=variance2 + (value2*value2);
                covariance=covariance + (value1*value2);
            }
        }
        double sqrt=Math.sqrt(variance1 * variance2);
        result=covariance / sqrt;
        return result;
    }

    // correlation for time plot
    public static double correlation(PlotList list, double mean1, double mean2){
        double result=0.0;
        double variance1=0.0;
        double variance2=0.0;
        double covariance=0.0;
        for(int i=0;i<list.getNbPosition();i++){
            double value1=(list.getList().elementAt(i).getXx() - mean1);
            double value2=(list.getList().elementAt(i).getYy() - mean2);
            variance1=variance1 + Math.sqrt(value1);
            variance2=variance2 + Math.sqrt(value2);
            covariance=covariance + (value1*value2);
        }
        result=covariance / Math.sqrt(variance1*variance2);
        return result;
    }

    public static double stdDev(PlotList list, double mean){
        double variance=variance(list,mean);
        return Math.sqrt(variance);
    }



    public static double variance(PlotList list, double mean){
        double sum=0.0;
        double nb;
        for(int i=0;i<list.getNbPosition();i++){
            nb= list.getList().elementAt(i).getStatValue() - mean;
            sum=sum + Math.sqrt(nb);
        }
        return sum/list.getNbPosition();
    }


    public static double mean(double sum, int population){
        double mean=0.0;
        mean=sum / population;
        return mean;
    }

    public static double[] slope(PlotList list){
        double sumX=0.0;
        double sumY=0.0;
        double xx=0.0;
        double yy=0.0;
        double xy=0.0;
        double slope1=0.0;
        double slope2=0.0;
        for(int i=0;i<list.getNbPosition();i++){
            sumX += list.getList().elementAt(i).getXx();
            sumY += list.getList().elementAt(i).getYy();
            xx += (list.getList().elementAt(i).getXx() * list.getList().elementAt(i).getXx());
            yy += (list.getList().elementAt(i).getYy() * list.getList().elementAt(i).getYy());
            xy += (list.getList().elementAt(i).getXx() * list.getList().elementAt(i).getYy());
        }
        double num=sumX * sumY;
        num=num/list.getNbPosition();
        num=xy - num;
        double denom1=sumX * sumX;
        denom1=denom1 / list.getNbPosition();
        denom1=xx - denom1;
        slope1=num/denom1;
        double denom2=Math.sqrt(sumY);
        denom2=denom2/list.getNbPosition();
        denom2=yy - denom2;
        slope2=num/denom2;
        double[] array=new double[2];
        array[0]=slope1;
        array[1]=slope2;
        return array;
    }

    public static double[] intercept(PlotList list, double[] slope){
        double sumX=0.0;
        double sumY=0.0;
        double intercept1=0.0;
        double intercept2=0.0;
        for(int i=0;i<list.getNbPosition();i++){
            sumX=sumX + list.getList().elementAt(i).getXx();
            sumY=sumY + list.getList().elementAt(i).getYy();
        }
        intercept1=sumX * slope[0];
        intercept1=sumY - intercept1;
        intercept1=intercept1/list.getNbPosition();

        intercept2=sumY * slope[1];
        intercept2=sumX - intercept2;
        intercept2=intercept2 / list.getNbPosition();

        double[] array=new double[2];
        array[0]=intercept1;
        array[1]=intercept2;
        return array;
    }
}