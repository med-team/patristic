/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author  Fourment Mathieu
 */
package Patristic;
   import java.awt.*;
   import java.awt.event.*;
   import javax.swing.*;
   import javax.swing.event.*;
   import java.util.*;
   import javax.swing.text.*;
   import javax.swing.table.TableColumn;
   import javax.swing.table.*;
   
/**
 * GUI used for the graphic display of the results
 */
   
   public class FrameDate extends JFrame implements TableModelListener  {
    public JScrollPane scroll;
    private JTextPane textPane =new JTextPane();
    String[][] data;
    String[] columnNames = {"Tip","Data"};
    TableModel model;
    Matrix list;
    
    /** Creates a new instance of Diagram */
    public FrameDate(Matrix list){
        this.list=list;
        setSize(600,400);
        this.setResizable(true);
        setTitle("Data: "+list.getFileName());
        Container pane = this.getContentPane();
        data=new String[list.getNames().length][2];
        //String[] date={"1933","1947","1998","1997","1996","1995","2000","2000","2002","1947","1992","2002","1989","1988","1980","1977","1934","1986","1943","1943","1979","1992"};
        for(int j=0;j<list.getNames().length;j++){
            data[j][0]=list.getNames()[j];
            data[j][1]=list.getDate()[j];
        }
        JTable table = new JTable(data, columnNames);
        model=table.getModel();
        model.addTableModelListener(this);
        

         scroll= new JScrollPane(table);
        pane.add(scroll,"Center");
   }
    
    public void tableChanged(TableModelEvent e) {
        String[] date=list.getDate();
        int row = e.getFirstRow();
        int column = e.getColumn();
        String columnName = model.getColumnName(column);
        Object data = model.getValueAt(row, column);
        if(column==1){
            date[row]=(String)data;
            this.list.setDate(date);
        }
    }
    
}


