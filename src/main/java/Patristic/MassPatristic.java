/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Patristic;

import java.io.*;
import java.util.*;


/**
 *
 * @author mathieu
 */
public class MassPatristic {
    static int nbTip=0;
    static int nbName=0;
    static Vector tempName=new Vector();
    static BufferedReader in=null;
    static BufferedWriter out=null;
    static Matrix list2=null;
    static Matrix list1=null;
    
    /** Creates a new instance of MassPatristic */
    public MassPatristic() {
    }
    
    public static void main(String[] args) {
        try{
            in = new BufferedReader(new FileReader("/home/mathieu/Desktop/tree.txt"));
            out = new BufferedWriter(new FileWriter("/home/mathieu/Desktop/result.txt"));
            String line="";
            while( (line=in.readLine()) !=null){
                if(line.length()>0){
                    String[] trees=line.split("@");
                    Object[] tree1=stringToArray(trees[1],false);
                    Object[] tree2=stringToArray(trees[2],false);
                    String[] columnNames1=new String[nbTip];
                    String[] columnNames2=new String[nbTip];
                    for(int j=0;j<nbName;j++) columnNames1[j]=(String)tempName.elementAt(j);
                    for(int j=0;j<nbName;j++) columnNames2[j]=(String)tempName.elementAt(j);
                    
                    list1=PatristicComputer.getDistances(tree1, columnNames1, "");
                    list2=PatristicComputer.getDistances(tree2, columnNames2, "");
                    double correlation=init();
                    out.write(trees[0]+","+String.valueOf(correlation)+"\n");
                }
            }
            in.close();
            out.close();
        }catch(IOException e){System.err.println(e.getMessage());}
    }
    
    private static Object[] stringToArray(String str, boolean nexus){
        Vector arrayTemp=new Vector();
        tempName=new Vector();
        int cellNb=-1;
        String wordTemp=""; //branch length or tip name
        nbTip=0;
        nbName=0;
        boolean bootstrap=false;
        for(int i=0;i<str.length();i++){
            if( str.substring(i,i+1).compareTo(":")==0 || str.substring(i,i+1).compareTo(",")==0 || str.substring(i,i+1).compareTo("(")==0 || str.substring(i,i+1).compareTo(")")==0 || str.substring(i,i+1).compareTo(";")==0 ){
                cellNb++;
                if (wordTemp.compareTo("")!=0) {
                    if( str.substring(i,i+1).compareTo(":")==0){
                        if(!nexus){
                            if(!arrayTemp.lastElement().equals(")")){
                                tempName.add(wordTemp);
                                nbName++;
                                arrayTemp.add(cellNb,String.valueOf(nbName));
                                nbTip++;
                            } else{
                                arrayTemp.add(cellNb,wordTemp);
                            }
                            cellNb++;
                        } else{
                            if(!arrayTemp.lastElement().equals(")")){
                                nbTip++;
                            }
                            arrayTemp.add(cellNb,wordTemp);
                            cellNb++;
                        }
                    }
                    // Add : to the array
                    else{
                        arrayTemp.add(cellNb,wordTemp);
                        cellNb++;
                    }
                }
                // ( ) , :
                // reset wordTemp
                wordTemp="";
                arrayTemp.add(cellNb,str.substring(i,i+1));
                
            }
            // temporary word: could be bootstrap, branch lenght
            // internal branch length or name
            else {wordTemp+=str.substring(i,i+1);}
        }
        return arrayTemp.toArray();
    }
    
    static private double init(){
        double correlation=0;
        double rateMutation=0;
        double invRate=0;
        double[][] matrix1=list1.getMatrix();
        double[][] matrix2=list2.getMatrix();
        reorder();
        matrix2=list2.getMatrix();
        int population=0;
        
        double sum1=0;
        double sum2=0;
        
        // look for max distance for X and Y
        // look for the min distance for replacing a distance=0
        for(int i=0;i<list1.getNames().length;i++){
            for(int j=i+1;j<list1.getNames().length;j++){
                population++;
                sum1=sum1+matrix1[i][j];
                sum2=sum2+matrix2[i][j];
            }
        }
        
        double mean1=MathStat.mean(sum1,population);
        double mean2=MathStat.mean(sum2,population);
        
        correlation=MathStat.correlation(list1, list2, mean1 ,mean2);
        
        double sbl1=list1.getSBL();
        double sbl2=list2.getSBL();
        rateMutation=sbl1/sbl2;
        invRate= 1.0000/(sbl1/sbl2);
        return correlation;
    }
    
    private static void reorder(){
        String[] names1=list1.getNames();
        String[] names2=list2.getNames();
        
        double [][] temp = new double[names1.length][names1.length];
        double [][] temp2 = new double[names1.length][names1.length];
        boolean diff=false;
        boolean doOrder=false;
        for(int i=0;i<names1.length;i++) {
            if(!names1[i].equals(names2[i])){diff=true;break;}
        }
        if(diff)list2=list2.reOrder(names1);
    }
}
