/*
 * Utils.java
 *
 * Created on November 6, 2007, 2:55 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Patristic;

import Patristic.tree.Node;
//import Patristic.tree.NodeI;
import Patristic.tree.Tree;
import java.util.Stack;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author mathieu
 */
public class Utils {

    public static void main(String[] args) {
        try{
          createTree("(O3-plasmodium_ovale-variant:0.00000466362223716875,(O2-plasmodium_ovale-classic:0.00126195921710415843,(451-plasmodium-chimp-afrique:0.00487876696254162072,(((P.knowlesi-AY598141-plasmodium-primate-asie:0.00718818500310487805,(P.cynomolgi-AY800108-plasmodium-primate-asie:0.00263950055935591047,((P.vivax-AY791692-plasmodium-humain:0.00000466362223716875,P.simium-AY800110-plasmodium-primate-amerique:0.00192438935255143605):0.00351838869448305733,P.simiovale-AY800109-plasmodium-primate-asie:0.00449516516821770080):0.00199521391646449069):0.00273980446886094983):0.01147284335820578094,(P.sp-AY800112-plasmodium-primate-afrique:0.03775201039135476361,P.gonderi-AY800111-plasmodium-primate-afique:0.00000466362223716875):0.00742284699398881257):0.01980809631241068833,((((P.berghei-AF014115-plasmodium-rongeur-afrique:0.01999130662882202258,P.yoelii-M29000-plasmodium-rongeur-afrique:0.01268237956981407150):0.04148201445605894516,P.chabaudi-AF014116-plasmodium-rongeur-afrique:0.04649476649389892163):0.09681707014790703891,((GOR601_GOR60-plasmodium-gorille-afrique:0.04989484504736837328,(((P._reichenowi-plasmodium-chimp-afrique:0.00000466362223716875,CPZ11-plasmodium-chimp-afrique:0.00000466362223716875):0.00000466362223716875,((P.falciparum-M76611-plasmodium-humain:0.00000466362223716875,CPZCAM46-Falci-plasmodium-chimp-afrique:0.00000466362223716875):0.00397631563268734285,GOR233-plasmodium-gorille-afrique:0.00000466362223716875):0.00839980114554037156):0.01473234883847622870,(CPZCAM80-plasmodium-chimp-afrique:0.00392444865443401097,CPZCAM155-plasmodium-chimp-afrique:0.00198594065399515084):0.04383925465428593904):0.01822479435774569320):0.15188430709896452719,PVV3-plasmodium-lemurien-madagascar:0.04023745192024671957):0.01172453049004969564):0.01772976014105049486,((P.malariae-AF182848-plasmodium-humain:0.00196751093825919517,CPZ59-plasmodium-chimp-afrique:0.00000466362223716875):0.05572704446459662841,((H.sp.haemoproteus-oiseau:0.14497579023874784809,L.caulleryi-leucocytozoon-oiseau:0.28639270751430750961):0.02033048569185628637,((P.gallinaceum-plasmodium-oiseau:0.03319686829868671168,P.relictum-plasmodium-oiseau:0.01286431251256971338):0.01109421805650474130,P.juxtanucleare-plasmodium-oiseau:0.02635852126080347885):0.03167008826762950785):0.22664662165729110299):0.00566282119868752234):0.00680280873402239021):0.00948378078516416700):0.00212188595905535467):0.00485472273940241485,225-plasmodium-chimp-afrique:0.00000466362223716875):0;");
        }catch(Exception e){e.printStackTrace();}
    }
    /** Creates a new instance of Utils */
    public Utils() {
    }
     public static Tree createTree(String newick) throws Exception{
        Vector<Node> taxa=new Vector<Node>();
        String[] newickArray=stringToArray(newick);
        Stack<Node> stack = new Stack<Node>();
        boolean isBoot=false;
        
        Pattern p = Pattern.compile("^:(.+)\\[(.+)\\]$");
        Matcher m = p.matcher("");

        int i=0;
        while(!newickArray[i].equals(";")){
            if(newickArray[i].equals("(")){
                Node node=new Node();
                
                if(!stack.empty()){
                    stack.peek().getChildren().addElement(node);
                    node.setParent(stack.peek());
                }
                stack.push(node);
                
                //System.out.print(names[namesID-1]);
                //print_stack(stack);
            } else if(newickArray[i].equals(")")){
                if(newickArray[i+1].equals(";")) break;
                // if there is a length branch
                if(newickArray[i+1].startsWith(":")){
                    // there is a bootstrap value  ex :0.01[95]
                    m.reset(newickArray[i+1]);
                    if(m.find()){
                        stack.peek().setBrlens(Double.valueOf(m.group(1)).doubleValue());
                        stack.peek().setBootstrap(Double.valueOf(m.group(2)).doubleValue());
                        isBoot=true;
                    } else
                        stack.peek().setBrlens(Double.valueOf(newickArray[i+1].substring(1)).doubleValue());
                    i++;
                }
                // does not start with a : but : is present => bootstrap value at the beginning ex 95:0.01
                else if(newickArray[i+1].contains(":")){
                    String[] temp=newickArray[i+1].split(":");
                    stack.peek().setBrlens(Double.valueOf(temp[1]).doubleValue());
                    stack.peek().setBootstrap(Double.valueOf(temp[0]).doubleValue());
                    i++;
                    isBoot=true;
                }
                stack.pop();
            } else if(newickArray[i].equals(",")){
                
            } else{
                String[] split=newickArray[i].split(":");
                Node node=new Node(split[0]);
                taxa.addElement(node);
                // There is no branch length
                if(split.length==1){
                    
                } else{
                    node.setBrlens(Double.valueOf(split[1]).doubleValue());
                }
                stack.peek().getChildren().addElement(node);
                node.setParent(stack.peek());
            }
            i++;
        }
        Tree tree=new Tree(newick);
        tree.setTree(stack.pop());
        if(newick.contains(":")) tree.isBrlens=true;
        tree.isBootstrap = isBoot;
        tree.setTaxa(taxa);
        return tree;
    }
     
     private static String[] stringToArray(String str) throws Exception{
        Vector<String> arrayTemp=new Vector<String>();
        str=str.trim();
        //System.out.println(str.substring(str.length()-20));
        String wordTemp=""; //branch length or tip name
        int bracket=0;
        str=str.substring(0, str.lastIndexOf(")")+1); // to delete :0.0 in ):0.0;
        str+=";";
        //System.out.println(str.substring(str.length()-20));
        for(int i=0;i<str.length();i++){
            if(str.charAt(i) == ',' || str.charAt(i) == '(' || str.charAt(i) == ')' || str.charAt(i) == ';' ){
                //if( str.charAt(i)==':' || str.substring(i,i+1).compareTo(",")==0 || str.substring(i,i+1).compareTo("(")==0 || str.substring(i,i+1).compareTo(")")==0 || str.substring(i,i+1).compareTo(";")==0 ){
                if (wordTemp.compareTo("")!=0) arrayTemp.add(wordTemp);
                wordTemp="";
                arrayTemp.add(str.substring(i,i+1));
                if(str.charAt(i) == '(') bracket++;
                else if(str.charAt(i) == ')') bracket--;
            } else wordTemp+=str.substring(i,i+1);
        }
        if(bracket>0) throw new Exception("Newick tree malformated: more (");
        else if(bracket<0) throw new Exception("Newick tree malformated: more )");    
        return (String[])arrayTemp.toArray(new String[arrayTemp.size()]);
    }
}
