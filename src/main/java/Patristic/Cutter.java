/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
package Patristic;
import java.util.*;
import java.io.*;
import java.util.regex.*;

/**
 *
 * @author  formmat
 */
public class Cutter {
    String fileName="";
    String cmd="";
    int win;
    int step;
    BufferedReader in;
    Pattern pattern = Pattern.compile("nchar=");
    Matcher matcher = pattern.matcher("");
    String path="";
    
    //static String path="C:\\Documents and Settings\\formmat\\Desktop\\Hantavirus\\L\\Cutter\\";
    //static String path="C:\\Documents and Settings\\formmat\\Desktop\\HIV\\";
    //static String path="C:\\Documents and Settings\\formmat\\Desktop\\Desktop\\Ebola\\NP\\Cutter\\";
    
    //int nbChar=0;
    
//        String PAUPScriptFirst="nj;"+"\r\n"+"savetrees file=result.nex brlens=yes replace;"+"\r\n";
//        String PAUPScriptRest ="nj;"+"\r\n"+"savetrees file=result.nex brlens=yes append;"+"\r\n";
    String PAUPScriptFirst="nj;"+"\r\n"+"savetrees file=result.nex brlens=yes replace;"+"\r\n";
    String PAUPScriptRest ="nj;"+"\r\n"+"savetrees file=result.nex brlens=yes append;"+"\r\n";
//    String ml="nj;\r\nset criterion=like;\r\n"+
//            "lscores 1/nst=6  basefreq=est rmatrix=estimate rates=gamma  shape=est pinv=est;\r\n"+
//            "lset nst=6  basefreq=previous rmatrix=previous rates=gamma  shape=previous pinv=previous;\r\n"+
//            "lscores 1/nst=6  basefreq=est rmatrix=estimate rates=gamma  shape=est pinv=est;\r\n"+
//            "lset nst=6  basefreq=previous rmatrix=previous rates=gamma  shape=previous pinv=previous;\r\n";
//
//    String PAUPScriptFirst="bootstrap;"+"\r\n"+"savetrees file=result.nex brlens=yes savebootp=nodelabels from=1 to=1 replace;"+"\r\n";
//    String PAUPScriptRest ="bootstrap;"+"\r\n"+"savetrees file=result.nex brlens=yes savebootp=nodelabels from=1 to=1 append;"+"\r\n";
    
    /** Creates a new instance of Paup */
    public Cutter(String path,int win, int step,String cmd) {
        this.win=win;
        this.step=step;
        this.cmd=cmd;
        String fileSeparator = System.getProperty("file.separator");
        this.path=path.substring(0, path.lastIndexOf(fileSeparator)+1);
        fileName=path.substring(path.lastIndexOf(fileSeparator)+1);
    }
    
    public int readNexus(String path){
        int nbChar=0;
        try{
            in = new BufferedReader(new FileReader(path+fileName));
            String line="";
            while(true){
                line=in.readLine();
                if(line==null)break;
                matcher.reset(line);
                if (matcher.find()) {
                    int start=line.indexOf("nchar=")+5;
                    nbChar=Integer.parseInt(line.substring(start+1,line.length()-1));
                }
            }
            in.close();
        } catch (IOException err) {System.out.println("IO "+err.getMessage());}
        return nbChar;
    }
    
    public void createOneFile(){
        Pattern pattern = Pattern.compile("tree");
        Matcher matcher = pattern.matcher("");
        final String SPACE = "[ \t\f\n\r]+";
        Pattern pattern1 = Pattern.compile("=");
        Matcher matcher1 = pattern1.matcher("");
        boolean eof=false;
        boolean translateFlag=false;
        boolean treeFlag=false;
        Vector array=new Vector();
        int length=0;
        
        Vector trees=new Vector();
        Vector name=new Vector();
        String line="";
        boolean addHead=true;
        String head="";
        
        boolean isFirst=true;
        try{
            in = new BufferedReader(new FileReader(path+"result.nex"));
            while((line = in.readLine())!=null){
                String lline=line.toLowerCase();
                //System.out.println(lline);
                matcher.reset(lline);
                matcher1.reset(lline);
                if(addHead){
                    head+=line+"\r\n";
                }
                //System.out.println(line);
                if (matcher.find() && matcher1.find() && treeFlag ) {
                    //System.out.println(line);
                    int pos=0;
                    String charr="z";
                    while(charr.compareTo("(")!=0){
                        charr=line.substring(pos,pos+1);
                        pos++;
                    }
                    pos--;
                    trees.add(line);
                    treeFlag=false;
                    ////////////eof=true;
                } else if(lline.matches("\ttranslate")){
                    translateFlag=true;
                    isFirst=false;
                } else if(line.matches("\t*;") && translateFlag==true){
                    translateFlag=false;
                    treeFlag=true;
                    addHead=false;
                } else if(translateFlag){
                    length++;
                    String str;
                    name.add(line.split(SPACE)[2]);//System.out.println(str);
                    //array.add(str.substring(0,str.length()-1));
                }
            }
            
            BufferedWriter out = new BufferedWriter(new FileWriter(path+"result1file.nex"));
            out.write(head);
            for(int i=0;i<trees.size();i++)out.write((String)trees.elementAt(i)+"\r\n");
            out.write("END;");
            out.close();
        } catch (IOException err) {System.out.println("IO "+err.getMessage());}
        
        
    }
    
    
    public void createCmd(){
        int nbChar=readNexus(path);
        int beginExclude=0;
        int endExclude=nbChar;
        int lastBeginExclude=beginExclude;
        int lastEndExclude=endExclude;
        int i=0;
        try{
            //BufferedWriter out = new BufferedWriter(new FileWriter(path+fileName+".cmd"));
            BufferedWriter out = new BufferedWriter(new FileWriter(path+"nexus.cmd"));
            System.out.println(path+"nexus.cmd");
            //out.write("BEGIN PAUP;\r\nexecute avian.nxs;\r\n\r\nset criterion=distance;\r\n\r\n");
            out.write("BEGIN PAUP;\r\nexecute '"+fileName+"';\r\n\r\n");
            
            while(true){
                if(beginExclude+win>nbChar){
                    int lastPos=lastBeginExclude+win;
                    int pos=beginExclude+win;
                    out.write("include "+1+"-"+lastBeginExclude+" "+lastPos+"-"+lastEndExclude+";\r\n");
                    out.write("exclude "+1+"-"+lastPos+";\r\n");
                    //out.write(PAUPScriptRest+"\r\n");
                    //out.write("nj;"+"\r\n"+"savetrees file=1-"+lastPos+".nex brlens=yes;"+"\r\n\r\n");
                    out.write(cmd+"savetrees file="+lastPos+"-"+lastEndExclude+".nex brlens=yes;"+"\r\n\r\n");
                    break;
                } else{
                    if(i==0){
                        int pos=1+win;
                        out.write("exclude "+ pos +"-"+endExclude+";\r\n");
                        //out.write(PAUPScriptFirst+"\r\n");
                        //out.write("nj;"+"\r\n"+"savetrees file=result"+pos+"-"+endExclude+".nex brlens=yes;"+"\r\n\r\n");
                        out.write(cmd+"savetrees file=1"+"-"+win+".nex brlens=yes;"+"\r\n\r\n");
                    } else if(i==1){
                        int pos=step+win;
                        int posx=1+win;
                        out.write("include "+posx+"-"+lastEndExclude+";\r\n");
                        out.write("exclude "+1+"-"+beginExclude+" "+pos+"-"+lastEndExclude+";\r\n");
                        //out.write(PAUPScriptRest+"\r\n");
                        //out.write("nj;"+"\r\n"+"savetrees file=result"+beginExclude+"-"+pos+".nex brlens=yes;"+"\r\n\r\n");
                        out.write(cmd+"savetrees file="+beginExclude+"-"+pos+".nex brlens=yes;"+"\r\n\r\n");
                    } else{
                        int lastPos=lastBeginExclude+win;
                        int pos=beginExclude+win;
                        out.write("include "+1+"-"+lastBeginExclude+" "+lastPos+"-"+lastEndExclude+";\r\n");
                        out.write("exclude "+1+"-"+beginExclude+" "+pos+"-"+lastEndExclude+";\r\n");
                        //out.write(PAUPScriptRest+"\r\n");
                        //out.write("nj;"+"\r\n"+"savetrees file=result"+beginExclude+"-"+pos+".nex brlens=yes;"+"\r\n\r\n");
                        out.write(cmd+"savetrees file="+beginExclude+"-"+pos+".nex brlens=yes;"+"\r\n\r\n");
                    }
                }
                lastBeginExclude=beginExclude;
                //lastEndExclude=endExclude;
                beginExclude+=step;
                endExclude+=win;
                i++;
            }
            out.write("END;");
            out.close();
        } catch (IOException err) {System.out.println("IO "+err.getMessage());}
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String path="C:\\Documents and Settings\\formmat\\Desktop\\Hantavirus\\L\\Cutter\\";
        String ml="nj;\r\nset criterion=like;\r\n"+
                "lscores 1/nst=6  basefreq=est rmatrix=estimate rates=gamma  shape=est pinv=est;\r\n"+
                "lset nst=6  basefreq=previous rmatrix=previous rates=gamma  shape=previous pinv=previous;\r\n"+
                "hs swap=nni;\r\n";
         if(args.length!=1){
	     System.out.println("One parameter please");
           return;
         }
        //new Cutter(path+"hiv.nxs",300,150,ml);
        
    }
    
}
