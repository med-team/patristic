/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author  formmat
 */
package Patristic;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

public class Option extends JFrame {

    private PlotModel plotModel;
    private JButton okButton,cancelButton,applyButton;
    private JLabel maxXLabel,minXLabel,unitXLabel,maxYLabel,minYLabel,unitYLabel;
    private JTextField maxXTextField,minXTextField,unitXTextField,maxYTextField,minYTextField,unitYTextField;
    private String maxX,minX,unitX,maxY,minY,unitY,dot,std,formula;
    private JPanel panelButton;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel dotPanel;
    private JLabel dotLabel;
    private JTextField dotTextField;
    private JTabbedPane jTabbedPane1;
    private JTextField formulaTextField,stdTextField;
    private JLabel formulaLabel,stdLabel;

    /** Creates new form OptionFrame */
    public Option(PlotModel optionObject) {
        this.plotModel=optionObject;
        init();
    }

    private void init() {
        setTitle("Options");
        panelButton = new JPanel();
        okButton = new JButton("OK");
        cancelButton = new JButton("Cancel");
        applyButton = new JButton("Apply");
        jTabbedPane1 = new JTabbedPane();
        jPanel2 = new JPanel();
        jPanel3 = new JPanel();
        formulaLabel = new javax.swing.JLabel("Formula:");
        formulaTextField = new JTextField("(x-y)");
        stdLabel = new javax.swing.JLabel("Standard deviation:");
        stdTextField = new javax.swing.JTextField("3");

        maxXLabel = new JLabel("Max X");
        maxXTextField = new JTextField("-1");
        minXLabel = new JLabel("Min X");
        minXTextField = new JTextField("-1");
        unitXLabel = new JLabel("Unit X");
        unitXTextField = new JTextField("1");

        maxYLabel = new JLabel("Max Y");
        maxYTextField = new JTextField("-1");
        minYLabel = new JLabel("Min Y");
        minYTextField = new JTextField("-1");
        unitYLabel = new JLabel("Unit Y");
        unitYTextField = new JTextField("1");


        dotPanel = new JPanel();
        dotLabel = new JLabel("Dot size (pixel):");
        dotTextField = new JTextField(3);

        Container pane=this.getContentPane();

        maxXTextField.setText(String.valueOf(plotModel.getMaxXGraph()));
        maxX=String.valueOf(plotModel.getMaxXGraph());
        minXTextField.setText(String.valueOf(plotModel.getMinXGraph()));
        minX=String.valueOf(plotModel.getMinXGraph());
        unitXTextField.setText(String.valueOf(plotModel.getXUnit()));
        unitX=String.valueOf(plotModel.getXUnit());

        maxYTextField.setText(String.valueOf(plotModel.getMaxYGraph()));
        maxY=String.valueOf(plotModel.getMaxYGraph());
        minYTextField.setText(String.valueOf(plotModel.getMinYGraph()));
        minY=String.valueOf(plotModel.getMinYGraph());
        unitYTextField.setText(String.valueOf(plotModel.getYUnit()));
        unitY=String.valueOf(plotModel.getYUnit());

        formulaTextField.setText(plotModel.getFormula());
        formula=plotModel.getFormula();
        stdTextField.setText(plotModel.getStd());
        std=plotModel.getStd();
        dotTextField.setText(plotModel.getDotSize());
        dot=plotModel.getDotSize();

        setSize(250,200);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm();
            }
        });
        okButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                plotModel.updateScaleX(Double.parseDouble(maxXTextField.getText()), Double.parseDouble(minXTextField.getText()), Double.parseDouble(unitXTextField.getText()) );
                plotModel.updateScaleY(Double.parseDouble(maxYTextField.getText()), Double.parseDouble(minYTextField.getText()), Double.parseDouble(unitYTextField.getText()));
                plotModel.updateFormula(formulaTextField.getText());
                plotModel.updateSTD(stdTextField.getText());


                try{
                    int nb=Integer.valueOf(dotTextField.getText()).intValue();
                    if(nb<=0)throw new NumberFormatException();
                    plotModel.updateProperties(dotTextField.getText());
                    plotModel.fireUpdated();
                    exitForm();
                } catch(NumberFormatException err){}
            }
        });
        applyButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if(maxX.compareTo(maxXTextField.getText())!=0 || minX.compareTo(minXTextField.getText())!=0 || maxY.compareTo(maxYTextField.getText())!=0 || minY.compareTo(minYTextField.getText())!=0
                        || unitX.compareTo(unitXTextField.getText())!=0 || unitY.compareTo(unitYTextField.getText())!=0 ){

                    plotModel.updateScaleX(Double.parseDouble(maxXTextField.getText()),Double.parseDouble(minXTextField.getText()), Double.parseDouble(unitXTextField.getText()));
                    plotModel.updateScaleY(Double.parseDouble(maxYTextField.getText()),Double.parseDouble(minYTextField.getText()), Double.parseDouble(unitYTextField.getText()));
                }
                if(formula.compareTo(formulaTextField.getText())!=0){
                    plotModel.updateFormula(formulaTextField.getText());
                }
                if(std.compareTo(stdTextField.getText())!=0)plotModel.updateSTD(stdTextField.getText());
                if(dot.compareTo(dotTextField.getText())!=0){
                    plotModel.updateProperties(dotTextField.getText());
                }
                plotModel.fireUpdated();
            }
        });
        cancelButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                exitForm();
            }
        });

        jTabbedPane1.addTab("Statistic", jPanel2);
        jPanel2.setLayout(new java.awt.GridLayout(3, 2));
        jPanel2.add(formulaLabel);
        jPanel2.add(formulaTextField);
        jPanel2.add(stdLabel);
        jPanel2.add(stdTextField);

        jTabbedPane1.addTab("Scale", jPanel3);
        jPanel3.setLayout(new java.awt.GridLayout(3, 4));
        jPanel3.add(maxXLabel);
        jPanel3.add(maxXTextField);
        jPanel3.add(maxYLabel);
        jPanel3.add(maxYTextField);
        jPanel3.add(minXLabel);
        jPanel3.add(minXTextField);
        jPanel3.add(minYLabel);
        jPanel3.add(minYTextField);
        jPanel3.add(unitXLabel);
        jPanel3.add(unitXTextField);
        jPanel3.add(unitYLabel);
        jPanel3.add(unitYTextField);

        jTabbedPane1.addTab("Properties", dotPanel);
        dotPanel.add(dotLabel);
        dotPanel.add(dotTextField);

        pane.add(jTabbedPane1,"Center");

        panelButton.add(okButton);
        panelButton.add(cancelButton);
        panelButton.add(applyButton);
        
        pane.add(panelButton,"South");


    }

    private void exitForm() {
        this.dispose();
    }

}