/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Patristic;

import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

public class Table extends JTable{
    
    public Table(PatristicTableModel myTableModel){
        super(myTableModel);
    }
    
    
    public TableCellRenderer getCellRenderer(int row, int column) {
            TableColumn tableColumn = getColumnModel().getColumn(column);
            TableCellRenderer renderer = tableColumn.getCellRenderer();
            if (renderer == null) {
                    Class c = getColumnClass(column);
                    if( c.equals(Object.class) )
                    {
                            Object o = getValueAt(row,column);
                            if( o != null )
                                    c = getValueAt(row,column).getClass();
                    }
                    renderer = getDefaultRenderer(c);
            }
            return renderer;
    }

    public TableCellEditor getCellEditor(int row, int column) {
            TableColumn tableColumn = getColumnModel().getColumn(column);
            TableCellEditor editor = tableColumn.getCellEditor();
            if (editor == null) {
                    Class c = getColumnClass(column);
                    if( c.equals(Object.class) )
                    {
                            Object o = getValueAt(row,column);
                            if( o != null )
                                    c = getValueAt(row,column).getClass();
                    }
                    editor = getDefaultEditor(c);
            }
            return editor;
    }
}


