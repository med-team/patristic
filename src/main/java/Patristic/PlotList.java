/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 *
 * @author  formmat
 */
package Patristic;

import java.util.Vector;

public class PlotList {
    
    private Vector<PlotCoord> list;
    private int lengthList;
    
    /** Creates a new instance of PositionList */
    public PlotList() {
        list = new Vector<PlotCoord>();
        lengthList=0;
    }
    
    // return the list (vector)
    public Vector<PlotCoord> getList(){
        return list;
    }
    
    // Return the number of Position object
    public int getNbPosition(){
        return lengthList;
    }    

    
            // Add a Position object in a none ordered list
    public void add(String name1,String name2, int xx, int yy,double x, double y){
        PlotCoord coord=new PlotCoord(name1,name2,xx,yy,x,y);
        list.add(coord);
        lengthList++;
    }
    
}
