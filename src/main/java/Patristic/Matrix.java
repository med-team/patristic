/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Patristic;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * List of Position object
 */
public class Matrix {

  private String[] names;
  private String[] dates;
  private boolean isDate = false;
  private String fileName;
  private int nbTaxon;
  private double sbl=-999;
  private double[][] matrix;

  public Matrix(String[] names, String fileName) {
    nbTaxon = names.length;
    matrix = new double[nbTaxon][nbTaxon];
    this.names = names;
    this.fileName = fileName;
    this.dates = new String[nbTaxon];
    for (int i = 0; i < nbTaxon; i++) {
      this.dates[i] = String.valueOf("");
    }
  }

  public Matrix reOrder(String[] newOrder) {
    double[][] temp = new double[names.length][names.length];
    double[][] temp2 = new double[names.length][names.length];
    String[] tempDate = new String[names.length];

    // Reorder time
    for (int j = 0; j < newOrder.length; j++) {
      if (newOrder[j].equals(names[j])) {
        tempDate[j] = dates[j];
      } else {
        for (int u = 0; u < newOrder.length; u++) {
          if (newOrder[j].equals(names[u])) {
            tempDate[j] = dates[u];
            break;
          }
        }
      }
      System.out.println(tempDate[j]);
    }
    dates = tempDate;

    // Horizontal
    for (int j = 0; j < newOrder.length; j++) {
      for (int x = 0; x < names.length; x++) {
        if (newOrder[j].equals(names[x])) {
          for (int z = 0; z < newOrder.length; z++) {
            temp[z][j] = matrix[z][x];
          }
          break;
        }
      }
    }
    // Vertical
    for (int j = 0; j < newOrder.length; j++) {
      for (int x = 0; x < names.length; x++) {
        if (newOrder[j].equals(names[x])) {
          for (int z = 0; z < newOrder.length; z++) {
            temp2[j][z] = temp[x][z];
          }
          break;
        }
      }
    }

    matrix = temp2;
    names = newOrder;
    return this;
  }

  private void moveRow(int from, int to) {
    double[] row = new double[nbTaxon];
    System.arraycopy(matrix[from], 0, row, 0, nbTaxon);
    if (from < to) {
      int pos = from + 1;
      while (pos <= to) {
        System.arraycopy(matrix[pos], 0, matrix[pos - 1], 0, nbTaxon);
        if (pos == to) {
          System.arraycopy(row, 0, matrix[pos], 0, nbTaxon);
        }
        pos++;
      }
    } else {
      int pos = from - 1;
      while (pos >= to) {
        System.arraycopy(matrix[pos], 0, matrix[pos + 1], 0, nbTaxon);
        if (pos == to) {
          System.arraycopy(row, 0, matrix[pos], 0, nbTaxon);
        }
        pos--;
      }
    }
  }

  private void moveNames(int from, int to) {
    String name = names[from];
    if (from < to) {
      int pos = from + 1;
      while (pos <= to) {
        names[pos - 1] = names[pos];
        if (pos == to) {
          names[pos] = name;
        }
        pos++;
      }
    } else {
      int pos = from - 1;
      while (pos >= to) {
        names[pos + 1] = names[pos];
        if (pos == to) {
          names[pos] = name;
        }
        pos--;
      }
    }
  }

  private synchronized void moveDates(int from, int to) {
    String d = dates[from];
    if (from < to) {
      int pos = from + 1;
      while (pos <= to) {
        dates[pos - 1] = dates[pos];
        if (pos == to) {
          dates[pos] = d;
        }
        pos++;
      }
    } else {
      int pos = from - 1;
      while (pos >= to) {
        dates[pos + 1] = dates[pos];
        if (pos == to) {
          dates[pos] = d;
        }
        pos--;
      }
    }
  }

  public void move(int from, int to) {
    synchronized (this) {
      moveColumn(from, to);
      moveRow(from, to);
      moveNames(from, to);
      if (isDate == true) {
        moveDates(from, to);
      }
    }
  }

  private void moveColumn(int from, int to) {
    double[] col = new double[nbTaxon];
    for (int i = 0; i < nbTaxon; i++) {
      col[i] = matrix[i][from];
    }
    if (from < to) {
      int pos = from + 1;
      while (pos <= to) {
        for (int i = 0; i < nbTaxon; i++) {
          matrix[i][pos - 1] = matrix[i][pos];
        }
        if (pos == to) {
          for (int i = 0; i < nbTaxon; i++) {
            matrix[i][pos] = col[i];
          }
        }
        pos++;
      }
    } else {
      int pos = from - 1;
      while (pos >= to) {
        for (int i = 0; i < nbTaxon; i++) {
          matrix[i][pos + 1] = matrix[i][pos];
        }
        if (pos == to) {
          for (int i = 0; i < nbTaxon; i++) {
            matrix[i][pos] = col[i];
          }
        }
        pos--;
      }
    }
  }

  public void toFile(BufferedWriter writer) throws IOException {
    for (int j = 0; j < names.length; j++) {
      writer.write("," + names[j]);
    }
    writer.newLine();
    for (int i = 0; i < nbTaxon; i++) {
      writer.write(names[i] + ",");
      for (int j = 0; j < nbTaxon; j++) {
        writer.write(matrix[i][j] + ",");
      }
      writer.newLine();
    }
  }

  public void toFileColumn(BufferedWriter writer, String format) throws IOException {
    for (int i = 0; i < nbTaxon; i++) {
      for (int j = i + 1; j < nbTaxon; j++) {
        if (format.compareTo("line") == 0) {
          writer.write(names[i] + "," + names[j] + "," + matrix[i][j] + ",");
          writer.newLine();
        } else if (format.compareTo("step") == 0) {
        } else if (format.compareTo("time") == 0) {
          int one = 0;
          int two = 0;
          for (int k = 0; k < names.length; k++) {
            if (names[i].compareTo(names[k]) == 0) {
              one = k;
            }
            if (names[j].compareTo(names[k]) == 0) {
              two = k;
            }
          }

          int date1 = new Integer(dates[one]).intValue();
          int date2 = new Integer(dates[two]).intValue();
          int diff = Math.abs(date1 - date2);
          writer.write(names[i] + "," + names[j] + "," + matrix[i][j] + "," + dates[one] + "," + dates[two] + "," + diff);
          writer.newLine();
        }
      }
    }
  }

  public void toColumnR(BufferedWriter writer) throws IOException {
    writer.write(fileName + ",");
    writer.newLine();
    for (int i = 0; i < nbTaxon; i++) {
      for (int j = 0; j < nbTaxon; j++) {
        writer.write(matrix[i][j] + ",");
        writer.newLine();
      }
    }
  }

  public void toDipFormat(BufferedWriter writer, boolean addNames) throws IOException {
    int space = 0;
    if (addNames) {
      for (int j = 0; j < names.length; j++) {
        writer.write(names[j]);
        writer.newLine();
      }
      writer.newLine();
    }
    writer.write(fileName);
    writer.newLine();

    for (int x = 0; x < names.length; x++) {
      if (x < names.length - 1) {
        for (int d = 0; d < space; d++) {
          writer.write("\t");
        }
      }
      space++;
      for (int j = x + 1; j < names.length; j++) {
        writer.write("\t" + matrix[x][j]);
      }
      if (x < names.length - 1) {
        writer.newLine();
      }
    }
  }

  public void setDate(String[] date) {
    this.dates = date;
    isDate = true;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public void setMatrix(double[][] matrix) {
    this.matrix = matrix;
  }

  public void setSbl(double sbl) {
    this.sbl = sbl;
  }

  public void setNames(String[] names) {
    this.names = names;
    nbTaxon = names.length;
  }

  public double[][] getMatrix() {
    return matrix;
  }

  public String getFileName() {
    return fileName;
  }

  public String[] getNames() {
    return names;
  }

  public String[] getDate() {
    return dates;
  }

  public boolean isDate() {
    return isDate;
  }

  public double getSBL() {
    return sbl;
  }
}
