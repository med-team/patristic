/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Patristic;

import javax.swing.table.*;
import java.util.*;
import javax.swing.event.*;
import java.awt.event.*;

public class PatristicTableModel extends DefaultTableModel {

  public void addRow(Matrix positionList) {
    Vector rowData = new Vector();
    final Matrix list = positionList;
    rowData.addElement(new Boolean(false));
    rowData.addElement(positionList);

    javax.swing.JButton button = new javax.swing.JButton("Show matrix");
    button.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        FrameMatrix frameMatrix = new FrameMatrix(list);
        frameMatrix.setVisible(true);
      }
    });
    rowData.addElement(button);

    dataVector.add(rowData);
    fireTableChanged(new TableModelEvent(this));
  }

  @Override
  public int getRowCount() {
    return dataVector.size();
  }

  @Override
  public Object getValueAt(int row, int col) {
    Object sr = "";
    if (col == 1) {
      sr = ((Matrix) ((Vector) dataVector.elementAt(row)).elementAt(1)).getFileName();
    } else if (col == 0) {
      sr = ((Vector) dataVector.elementAt(row)).elementAt(0);
    } else if (col == 2) {
      sr = ((Vector) dataVector.elementAt(row)).elementAt(2);
    } else {
      sr = ((Matrix) ((Vector) dataVector.elementAt(row)).elementAt(1));
    }
    return sr;
  }

  @Override
  public Class getColumnClass(int c) {
    return getValueAt(0, c).getClass();
  }

  
  @Override
  public boolean isCellEditable(int row, int col) {
    if (col == 1) {
      return false;
    } else {
      return true;
    }
  }
}
