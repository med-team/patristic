/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package Patristic;

import javax.swing.*;

/**
 *
 * @author fourmat
 */
class ProgressBar extends JFrame {
    public JProgressBar progressBar;
    
    public ProgressBar(int length,String file) {
        super("Loading...");
        progressBar=new JProgressBar(0, length); 
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        JPanel panel = new JPanel();
        panel.add(progressBar);
        panel.add(new JLabel("Loading "+file+"..."));
        this.getContentPane().add(panel);
        this.setSize(400,100);
    }
    
    
}
