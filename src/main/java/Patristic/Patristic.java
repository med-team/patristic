/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 *
 * @author  Fourment Mathieu
 */
package Patristic;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;

/**
 * Main GUI
 **/
public class Patristic extends JPanel {

  private JFrame frame = null;
  private PatristicApplet applet = null;
  private JMenuBar bar = new JMenuBar();
  private JMenu fileMenu = new JMenu("File");
  private JMenuItem open = new JMenuItem("Open tree");
  private JMenu subLoad = new JMenu("Import");
  private JMenuItem loadMega = new JMenuItem("Mega");
  private JMenuItem loadDip = new JMenuItem("DIP");
  private JMenuItem loadPhylip = new JMenuItem("Phylip");
  private JMenu subPaup = new JMenu("PAUP");
  private JMenu subLoadM = new JMenu("Matrix");
  private JMenuItem loadMatrix1A = new JMenuItem("Ajusted");
  private JMenuItem loadMatrix1P = new JMenuItem("Patristic");
  private JMenuItem load = new JMenuItem("Column");
  private JMenu subSave = new JMenu("Save");
  private JMenuItem saveMat = new JMenuItem("Save matrix");
  private JMenuItem saveCol = new JMenuItem("Save column");
  private JMenuItem saveStep = new JMenuItem("Save steps");
  private JMenuItem saveTime = new JMenuItem("Save time");
  private JMenuItem saveDip = new JMenuItem("Save DIP");
  private JMenuItem saveR = new JMenuItem("Save multiple column");
  private JMenuItem delete = new JMenuItem("Delete");
  private JMenuItem close = new JMenuItem("Close");
  private JMenu graphMenu = new JMenu("Plot");
  private JMenuItem showPlot = new JMenuItem("Distance vs. Distance");
  private JMenuItem showTime = new JMenuItem("Distance vs. Time");
  private JMenu editMenu = new JMenu("Edit");
  private JMenuItem polytomy = new JMenuItem("Polytomy");
  private JMenuItem editDate = new JMenuItem("Time");
  private JMenuItem loadDate = new JMenuItem("Load time");
  private JMenu helpMenu = new JMenu("Help");
  private JMenuItem help = new JMenuItem("Help");
  private JTextField motif = new JTextField(40);
  private JButton launch = new JButton("Analyse");
  private Object[] tree;
  private int nbTip = 0;
  private int treep = 0;
  Vector tempName;
  int nbName = 0;
  final String fileSeparator = System.getProperty("file.separator");
  String[] columnIdentifiers = {"tree", "filename", "Matrix"};
  //private TreeList list;
  private PatristicTableModel myTableModel;
  private Table table;
  private JScrollPane scrollInput;
  private File path = new File(".");
  private JToolBar toolBar = null;

  //WindowsTrayIcon icon;
//    javax.swing.Timer timer=null;
//    PatristicComputerThread task=null;
//    ProgressBar progress=null;
  public Patristic(PatristicApplet applet) {
    this(applet, null);
  }

  public Patristic(PatristicApplet applet, GraphicsConfiguration gc) {

    // Note that the applet may null if this is started as an application
    this.applet = applet;

    if (!isApplet()) {

      new net.iharder.dnd.FileDrop(motif, new net.iharder.dnd.FileDrop.Listener() {

        public void filesDropped(java.io.File[] files) {
          for (int i = 0; i < files.length; i++) {
            try {
              Opener open = new Opener(files[i].getCanonicalPath());
              path = new File(files[i].getAbsolutePath().substring(0, files[i].getCanonicalPath().lastIndexOf(fileSeparator)));
              String fileSeparator = System.getProperty("file.separator");
              String fileName = files[i].getCanonicalPath().substring(files[0].getCanonicalPath().lastIndexOf(fileSeparator) + 1);
              tree = stringToArray(open.getText(), open.isNexus());
              String[] columnNames;
              if (open.isNexus()) {
                columnNames = open.getName(nbTip);
              } else {
                columnNames = new String[nbTip];
                for (int j = 0; j < nbName; j++) {
                  columnNames[j] = (String) tempName.elementAt(j);
                }
              }
              Matrix mat = PatristicComputer.getDistances(tree, columnNames, fileName);
              myTableModel.addRow(mat);

              saveMat.setEnabled(true);
              saveCol.setEnabled(true);
              showTime.setEnabled(true);
              saveDip.setEnabled(true);
              saveR.setEnabled(true);
              saveTime.setEnabled(true);
              delete.setEnabled(true);
              saveStep.setEnabled(true);
            //text.append( files[i].getCanonicalPath() + "\n" );
            } // end try
            catch (java.io.IOException e) {
            }
          }   // end for: through each dropped file
        }   // end filesDropped
      }); // end FileDrop.Listener

      //frame = createFrame(gc);
      frame = new JFrame();
      frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
    //frame.setIconImage(loadImage("Duke16.gif"));

    }
    setLayout(new BorderLayout());

    setPreferredSize(new Dimension(600, 200));
    initialize();
    showPatristic();
  }

  public static JFrame createFrame(GraphicsConfiguration gc) {
    JFrame frame = new JFrame(gc);
    //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    return frame;
  }

  /** Creates a new instance of PatristicFrame */
  public void showPatristic() {
    myTableModel.setColumnIdentifiers(columnIdentifiers);
    table.setDefaultRenderer(JComponent.class, new JComponentCellRenderer());
    table.setDefaultEditor(JComponent.class, new JComponentCellEditor());

    if (!isApplet()) {
      frame.getContentPane().add(this, BorderLayout.CENTER);
      frame.setTitle("Patristic");
      frame.pack();
      frame.setVisible(true);
    }
  }

  public void initialize() {
    JPanel top = new JPanel();
    top.setLayout(new BorderLayout());
    add(top, BorderLayout.NORTH);

    bar = createBar();
    top.add(bar, BorderLayout.NORTH);
    toolBar = createToolBar();
    toolBar.setFloatable(false);
    toolBar.setRollover(true);
    top.add(toolBar, BorderLayout.CENTER);
    top.add(motif, BorderLayout.SOUTH);

    myTableModel = new PatristicTableModel();
    table = new Table(myTableModel);
    scrollInput = new JScrollPane(table);
    add(scrollInput, "Center");
  }

  // Create the menu bar
  private JMenuBar createBar() {
    JMenuBar menuBar = new JMenuBar();
    //setJMenuBar(bar);
    menuBar.add(fileMenu);

    fileMenu.add(open);
    open.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        openFile();
      }
    });

    fileMenu.add(subLoad);
    loadMega.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        loadMega();
      }
    });
    subLoad.add(loadMega);

    subLoad.add(loadDip);
    loadDip.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        loadDip();
      }
    });
    subLoad.add(loadPhylip);
    loadPhylip.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        loadPhylip();
      }
    });

    subLoad.add(subPaup);
    subLoadM.add(loadMatrix1A);
    loadMatrix1A.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        loadMatrix(Loader.UPPERMATRIX);
      }
    });
    subLoadM.add(loadMatrix1P);
    loadMatrix1P.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        loadMatrix(Loader.LOWERMATRIX);
      }
    });
    subPaup.add(subLoadM);

    subPaup.add(load);
    load.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        loadColumn();
      }
    });

    fileMenu.addSeparator();
    fileMenu.add(subSave);
    saveMat.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        save("matrix");
      }
    });
    saveMat.setEnabled(false);
    subSave.add(saveMat);

    subSave.add(saveTime);
    saveTime.setEnabled(false);
    saveTime.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        save("time");
      }
    });

    saveCol.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        save("line");
      }
    });
    saveCol.setEnabled(false);
    subSave.add(saveCol);


    saveR.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        saveR();
      }
    });
    saveR.setEnabled(false);
    subSave.add(saveR);

    saveDip.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        saveDip();
      }
    });
    saveDip.setEnabled(false);
    subSave.add(saveDip);

    fileMenu.addSeparator();
    fileMenu.add(delete);
    delete.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        delete();
      }
    });
    delete.setEnabled(false);

    fileMenu.addSeparator();
    fileMenu.add(close);
    close.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        close();
      }
    });

    menuBar.add(graphMenu);
    graphMenu.add(showPlot);
    showPlot.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        showPlot();
      }
    });

    editMenu.add(editDate);
    editDate.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        editDate();
      }
    });

    editMenu.add(loadDate);
    loadDate.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        loadDate();
      }
    });

    graphMenu.add(showTime);
    showTime.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        showTime();
      }
    });
    showTime.setEnabled(false);
    //        subShowDist.add(showDist2);
    //        showDist2.addActionListener(new ActionListener(){
    //            public void actionPerformed(ActionEvent e) {
    //            }
    //        });
    //        showDist.setEnabled(false);
    //        showDist2.setEnabled(false);
    //        graphMenu.add(subShowDist);

    menuBar.add(editMenu);
    editMenu.add(polytomy);
    polytomy.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        Matrix list = null;
        if (getNbRowSelected() == 1) {
          for (int i = 0; i < myTableModel.getRowCount(); i++) {
            if (table.getModel().getValueAt(i, 0).toString().compareTo("true") == 0) {
              list = (Matrix) myTableModel.getValueAt(i, -1);
              break;
            }
          }

          Polytomy frame = new Polytomy(list);
          frame.pack();
          frame.setVisible(true);
        }
      }
    });

    menuBar.add(helpMenu);
    helpMenu.add(help);
    help.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
      }
    });

    return menuBar;
  }

  protected JToolBar createToolBar() {
    JButton button = null;
    JToolBar toolBar = new JToolBar("ToolBar");

    //first button
    button = makeNavigationButton("Open16", "Load a tree", "Tree");
    toolBar.add(button);

    button = makeNavigationButton("Save16", "Save matrix", "Matrix");
    toolBar.add(button);

    button = makeNavigationButton("Delete16", "Delete tree", "Delete");
    toolBar.add(button);

    button = makeNavigationButton("Zoom16", "Show plot", "Plot");
    toolBar.add(button);

    button = makeNavigationButton("Edit16", "Edit Time", "edTime");
    toolBar.add(button);

    button = makeNavigationButton("Import16", "Compute", "compute");
    toolBar.add(button);

    return toolBar;
  }

  protected JButton makeNavigationButton(String imageName,
          String toolTipText,
          String altText) {
    String imgLocation = "/toolbarButtonGraphics/general/" + imageName + ".gif";
    URL imageURL = Patristic.class.getResource(imgLocation);
    //String loc = PatristicFrame.class.getR;
    //Create and initialize the button.
    JButton button = new JButton();
    button.setActionCommand(altText);
    button.setToolTipText(toolTipText);
    button.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Tree")) {
          openFile();
        } else if (e.getActionCommand().equals("Mega")) {
          loadMega();
        } else if (e.getActionCommand().equals("Matrix")) {
          save("matrix");
        } else if (e.getActionCommand().equals("Time")) {
          save("time");
        } else if (e.getActionCommand().equals("Column")) {
          save("line");
        } else if (e.getActionCommand().equals("Delete")) {
          delete();
        } else if (e.getActionCommand().equals("Plot")) {
          showPlotButton();
        } else if (e.getActionCommand().equals("edTime")) {
          editDate();
        } else if (e.getActionCommand().equals("compute")) {
          compute();
        }
      }
    });

    //button.setIcon(new ImageIcon("Forward24.gif", altText));
    if (imageURL != null) {                      //image found
      button.setIcon(new ImageIcon(imageURL, altText));
    //            System.err.println(imageURL+" Resource not found: " + imgLocation);
    } else {                                     //no image found
      button.setText(altText);
      System.err.println("Resource not found: " + imgLocation);
    }

    return button;
  }

  private Object[] stringToArray(String str, boolean nexus) {
    Vector arrayTemp = new Vector();
    tempName = new Vector();
    int cellNb = -1;
    String wordTemp = ""; //branch length or tip name
    nbTip = 0;
    nbName = 0;
    for (int i = 0; i < str.length(); i++) {
      if (str.substring(i, i + 1).compareTo(":") == 0 || str.substring(i, i + 1).compareTo(",") == 0 || str.substring(i, i + 1).compareTo("(") == 0 || str.substring(i, i + 1).compareTo(")") == 0 || str.substring(i, i + 1).compareTo(";") == 0) {
        cellNb++;
        if (wordTemp.compareTo("") != 0) {
          if (str.substring(i, i + 1).compareTo(":") == 0) {
            if (!nexus) {
              if (!arrayTemp.lastElement().equals(")")) {
                tempName.add(wordTemp);
                nbName++;
                arrayTemp.add(cellNb, String.valueOf(nbName));
                nbTip++;
              } else {
                arrayTemp.add(cellNb, wordTemp);
              }
              cellNb++;
            } else {
              if (!arrayTemp.lastElement().equals(")")) {
                nbTip++;
              }
              arrayTemp.add(cellNb, wordTemp);
              cellNb++;
            }
          } // Add : to the array
          else {
            arrayTemp.add(cellNb, wordTemp);
            cellNb++;
          }
        }
        // ( ) , :
        // reset wordTemp
        wordTemp = "";
        arrayTemp.add(cellNb, str.substring(i, i + 1));

      } // temporary word: could be bootstrap, branch lenght
      // internal branch length or name
      else {
        wordTemp += str.substring(i, i + 1);
      }
    }
    return arrayTemp.toArray();
  }

  private int getNbRowSelected() {
    int nbChecked = 0;
    for (int i = 0; i < myTableModel.getRowCount(); i++) {
      if (table.getModel().getValueAt(i, 0).toString().compareTo("true") == 0) {
        nbChecked++;
      }
    }
    return nbChecked;
  }

  private void loadDate() {
    Matrix theList = null;
    int nbTaxa = 0;
    String[] date;
    if (getNbRowSelected() == 1) {
      for (int i = 0; i < myTableModel.getRowCount(); i++) {
        if (table.getModel().getValueAt(i, 0).toString().compareTo("true") == 0) {
          theList = (Matrix) myTableModel.getValueAt(i, -1);
          break;
        }
      }
      nbTaxa = theList.getNames().length;
      date = new String[nbTaxa];


      JFileChooser opener = new JFileChooser();
      opener.setCurrentDirectory(path);
      opener.addChoosableFileFilter(new FiltreExtension("date", "Date file"));
      int returnVal = opener.showOpenDialog(this);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        String pathFilename = opener.getSelectedFile().toString();
        path = opener.getCurrentDirectory();
        try {
          String line = "";
          boolean eof = false;
          BufferedReader in = new BufferedReader(new FileReader(pathFilename));
          int i = 0;
          while (!eof) {
            line = in.readLine();
            if (line == null) {
              eof = true;
            } else if (line.equals("")) {
            } else {
              date[i] = line;
              i++;
            }

          }
          in.close();
          theList.setDate(date);

          FrameDate frameDate = new FrameDate(theList);
          frameDate.setVisible(true);
        } catch (IOException err) {
          System.out.println("IO " + err.getMessage());
        } catch (NullPointerException err) {
          err.printStackTrace();
        }
      }

    } else {
      JOptionPane.showMessageDialog(this, "1 tree is needed");
    }
  }

  // Open a file and load the sequence in the input textarea
  private void openFile() {
    final JFileChooser opener;
    //javax.swing.Timer timer;
    PatristicComputerThread task;
    ProgressBar progress;

    opener = new JFileChooser();
    opener.setMultiSelectionEnabled(true);
    opener.setCurrentDirectory(path);
    opener.addChoosableFileFilter(new FiltreExtension("nex", "Nexus file"));
    opener.addChoosableFileFilter(new FiltreExtension("ph", "Ph file"));
    opener.addChoosableFileFilter(new FiltreExtension("tre", "Tre file"));

    int returnVal = opener.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
//            Thread run= new Thread() {
//                public void run() {
      File[] files = opener.getSelectedFiles();
      for (int i = 0; i < files.length; i++) {
        //String pathFilename=opener.getSelectedFile().toString();
        path = opener.getCurrentDirectory();

        Opener open = new Opener(files[i].getAbsolutePath());

        String fileName = files[i].getAbsolutePath().substring(files[i].getAbsolutePath().lastIndexOf(fileSeparator) + 1);
        tree = stringToArray(open.getText(), open.isNexus());
        String[] columnNames;
        if (open.isNexus()) {
          columnNames = open.getName(nbTip);
        } else {
          columnNames = new String[nbTip];
          for (int j = 0; j < nbName; j++) {
            columnNames[j] = (String) tempName.elementAt(j);
          }
        }


        task = new PatristicComputerThread(this, tree, columnNames, fileName);
        progress = new ProgressBar(task.getLengthOfTask(), fileName);
        progress.setVisible(true);
        javax.swing.Timer timer = new javax.swing.Timer(1000, null);
        timer.addActionListener(new TimerListener(progress, task, timer));
        task.go();
        timer.start();

        //temp
        //Matrix m=PatristicComputer.compute(open.getText(),fileName);
        //myTableModel.addRow(m);

        saveMat.setEnabled(true);
        saveCol.setEnabled(true);
        showTime.setEnabled(true);
        saveDip.setEnabled(true);
        saveR.setEnabled(true);
        saveTime.setEnabled(true);
        delete.setEnabled(true);
        saveStep.setEnabled(true);
      // end tem

      }
    }
  }

  public JTable getTable() {
    return table;
  }

  private void loadMega() {
    Loader loader;
    String pathFilename = "";
    JFileChooser opener = new JFileChooser();
    opener.setCurrentDirectory(path);
    opener.addChoosableFileFilter(new FiltreExtension("mega", "Mega file"));

    int returnVal = opener.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      pathFilename = opener.getSelectedFile().toString();
      path = opener.getCurrentDirectory();
      loader = new Loader(pathFilename, Loader.MEGA);
      myTableModel.addRow(loader.getList());

      saveMat.setEnabled(true);
      saveCol.setEnabled(true);
      showTime.setEnabled(true);
      saveDip.setEnabled(true);
      saveR.setEnabled(true);
      saveTime.setEnabled(true);
      delete.setEnabled(true);
      saveStep.setEnabled(true);
    }

  }

  private void loadColumn() {
    String pathFilename = "";
    JFileChooser opener = new JFileChooser();
    opener.setCurrentDirectory(path);
    opener.addChoosableFileFilter(new FiltreExtension("nex", "Nexus file"));
    int returnVal = opener.showOpenDialog(this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      pathFilename = opener.getSelectedFile().toString();
      path = opener.getCurrentDirectory();
      Loader loader = new Loader(pathFilename, Loader.COLUMN);

      myTableModel.addRow(loader.getList());

      saveMat.setEnabled(true);
      saveCol.setEnabled(true);
      showTime.setEnabled(true);
      saveDip.setEnabled(true);
      saveR.setEnabled(true);
      saveTime.setEnabled(true);
      delete.setEnabled(true);
      saveStep.setEnabled(true);
    }
  }

  private void loadDip() {
    String pathFilename = "";
    JFileChooser opener = new JFileChooser();
    opener.setCurrentDirectory(path);
    opener.addChoosableFileFilter(new FiltreExtension("dip", "Dip file"));

    int returnVal = opener.showOpenDialog(this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      pathFilename = opener.getSelectedFile().toString();
      path = opener.getCurrentDirectory();
      Loader loader = new Loader(pathFilename, Loader.DIPLOMO);

      myTableModel.addRow(loader.getList());
      saveMat.setEnabled(true);
      saveCol.setEnabled(true);
      showTime.setEnabled(true);
      saveDip.setEnabled(true);
      saveR.setEnabled(true);
      saveTime.setEnabled(true);
      delete.setEnabled(true);
      saveStep.setEnabled(true);
    }
  }

  private void loadPhylip() {
    String pathFilename = "";
    JFileChooser opener = new JFileChooser();
    opener.setCurrentDirectory(path);
    opener.addChoosableFileFilter(new FiltreExtension("tre", "Phylip file"));

    int returnVal = opener.showOpenDialog(this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      pathFilename = opener.getSelectedFile().toString();
      path = opener.getCurrentDirectory();
      Loader loader = new Loader(pathFilename, Loader.PHYLIP);

      myTableModel.addRow(loader.getList());
      saveMat.setEnabled(true);
      saveCol.setEnabled(true);
      showTime.setEnabled(true);
      saveDip.setEnabled(true);
      saveR.setEnabled(true);
      saveTime.setEnabled(true);
      delete.setEnabled(true);
      saveStep.setEnabled(true);
    }
  }

  private void loadMatrix(int what) {
    String pathFilename = "";
    JFileChooser opener = new JFileChooser();
    opener.setCurrentDirectory(path);
    opener.addChoosableFileFilter(new FiltreExtension("txt", "Nexus file"));

    int returnVal = opener.showOpenDialog(this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      pathFilename = opener.getSelectedFile().toString();
      path = opener.getCurrentDirectory();
      Loader loader = new Loader(pathFilename, what);

      myTableModel.addRow(loader.getList());

      saveMat.setEnabled(true);
      saveCol.setEnabled(true);
      showTime.setEnabled(true);
      saveDip.setEnabled(true);
      saveR.setEnabled(true);
      saveTime.setEnabled(true);
      delete.setEnabled(true);
      saveStep.setEnabled(true);
    }
  }

  private void save(String format) {
    Matrix matrix = null;
    if (getNbRowSelected() == 1) {
      for (int i = 0; i < myTableModel.getRowCount(); i++) {
        if (table.getModel().getValueAt(i, 0).toString().compareTo("true") == 0) {
          matrix = (Matrix) myTableModel.getValueAt(i, -1);
          break;
        }
      }
      JFileChooser filechooser = new JFileChooser();
      filechooser.setCurrentDirectory(path);
      filechooser.addChoosableFileFilter(new FiltreExtension("csv", "CSV file"));
      int returnVal = filechooser.showSaveDialog(frame);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        String filename = filechooser.getSelectedFile().toString();
        if (!filename.endsWith(".csv")) {
          filename = filename + ".csv";
        }
        BufferedWriter writer = null;
        try {
          writer = new BufferedWriter(new FileWriter(filename));
          if (format.compareTo("matrix") == 0) {
            matrix.toFile(writer);
          } else {
            matrix.toFileColumn(writer, format);
          }
          path = filechooser.getCurrentDirectory();
        } catch (IOException ex) {
          ex.printStackTrace();
        } finally {
          try {
            if (writer != null) {
              writer.flush();
              writer.close();
            }

          } catch (IOException exx) {
            exx.printStackTrace();
          }
        }
      }
    } else {
      JOptionPane.showMessageDialog(this, "Select 1 tree");
    }
  }

  private void saveDip() {
    Matrix matrix = null;
    JFileChooser filechooser = new JFileChooser();
    filechooser.setCurrentDirectory(path);
    filechooser.addChoosableFileFilter(new FiltreExtension("csv", "CSV file"));
    int returnVal = filechooser.showSaveDialog(frame);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      String filename = filechooser.getSelectedFile().toString();
      BufferedWriter writer = null;
      try {
        writer = new BufferedWriter(new FileWriter(filename));

        int nb = getNbRowSelected();
        boolean addNames = true;
        if (nb >= 1) {
          for (int i = 0; i < myTableModel.getRowCount(); i++) {
            if (table.getModel().getValueAt(i, 0).toString().compareTo("true") == 0) {
              matrix = (Matrix) myTableModel.getValueAt(i, -1);
              matrix.toDipFormat(writer, addNames);
              if (nb > 1) {
                addNames = false;
              }
            }
          }
          path = filechooser.getCurrentDirectory();
        }
      } catch (IOException ex) {
        ex.printStackTrace();
      } finally {
        try {
          if (writer != null) {
            writer.flush();
            writer.close();
          }

        } catch (IOException exx) {
          exx.printStackTrace();
        }
      }
    } else {
      JOptionPane.showMessageDialog(this, "1 tree at least is needed");
    }

  }

  private void saveR() {
    Matrix matrix = null;
    JFileChooser filechooser = new JFileChooser();
    filechooser.setCurrentDirectory(path);
    filechooser.addChoosableFileFilter(new FiltreExtension("csv", "CSV file"));
    int returnVal = filechooser.showSaveDialog(frame);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      String filename = filechooser.getSelectedFile().toString();
      BufferedWriter writer = null;
      try {
        writer = new BufferedWriter(new FileWriter(filename));

        int nb = getNbRowSelected();
        int nbb = 0;
        writer.write("tax1,tax2");
        if (nb >= 1) {
          for (int i = 0; i < myTableModel.getRowCount(); i++) {
            if (table.getModel().getValueAt(i, 0).toString().compareTo("true") == 0) {
              matrix = (Matrix) myTableModel.getValueAt(i, -1);
              nbb = matrix.getNames().length;
              writer.write("," + matrix.getFileName());
            }
          }

          for (int j = 0; j < nbb; j++) {
            for (int k = j + 1; k < nbb; k++) {
              writer.newLine();
              writer.write(matrix.getNames()[j] + "," + matrix.getNames()[k] + ",");
              for (int i = 0; i < myTableModel.getRowCount(); i++) {
                if (table.getModel().getValueAt(i, 0).toString().compareTo("true") == 0) {
                  matrix = (Matrix) myTableModel.getValueAt(i, -1);
                  writer.write(matrix.getMatrix()[j][k] + ",");
                }
              }

            }
          }
        }
      } catch (IOException ex) {
        ex.printStackTrace();
      } finally {
        try {
          if (writer != null) {
            writer.flush();
            writer.close();
          }

        } catch (IOException exx) {
          exx.printStackTrace();
        }
      }
      path = filechooser.getCurrentDirectory();
    } else {
      JOptionPane.showMessageDialog(this, "1 tree at least is needed");
    }

  }

  private void editDate() {
    Matrix theList = null;
    if (getNbRowSelected() == 1) {
      for (int i = 0; i < myTableModel.getRowCount(); i++) {
        if (table.getModel().getValueAt(i, 0).toString().compareTo("true") == 0) {
          theList = (Matrix) myTableModel.getValueAt(i, -1);
          break;
        }
      }

      FrameDate frameDate = new FrameDate(theList);
      frameDate.setVisible(true);
    } else {
      JOptionPane.showMessageDialog(this, "1 tree is needed");
    }
  }

  private void delete() {
    int result = JOptionPane.showConfirmDialog(null, "Are you sure?", "", JOptionPane.YES_NO_OPTION);
    if (result == JOptionPane.YES_OPTION) {
      for (int i = myTableModel.getRowCount() - 1; i >= 0; i--) {
        if (table.getModel().getValueAt(i, 0).toString().compareTo("true") == 0) {
          myTableModel.removeRow(i);
        }
      }
    }
  }

  private void showPlotButton() {
    Matrix matrix1 = null;
    Matrix matrix2 = null;
    if (getNbRowSelected() == 1) {
      showTime();
    } else if (getNbRowSelected() == 2) {
      for (int i = 0; i < myTableModel.getRowCount(); i++) {
        if (table.getModel().getValueAt(i, 0).toString().compareTo("true") == 0) {
          if (matrix1 == null) {
            matrix1 = (Matrix) myTableModel.getValueAt(i, -1);
          } else {
            matrix2 = (Matrix) myTableModel.getValueAt(i, -1);
          }
        }
      }
      PlotFrame plot = new PlotFrame(matrix1, matrix2);
      plot.setVisible(true);
    } else {
      JOptionPane.showMessageDialog(this, "2 trees are needed");
    }
  }

  private void showPlot() {
    Matrix matrix1 = null;
    Matrix matrix2 = null;
    if (getNbRowSelected() == 2) {
      for (int i = 0; i < myTableModel.getRowCount(); i++) {
        if (table.getModel().getValueAt(i, 0).toString().compareTo("true") == 0) {
          if (matrix1 == null) {
            matrix1 = (Matrix) myTableModel.getValueAt(i, -1);
          } else {
            matrix2 = (Matrix) myTableModel.getValueAt(i, -1);
          }
        }
      }
      PlotFrame plot = new PlotFrame(matrix1, matrix2);
      plot.setVisible(true);
    } else if (myTableModel.getRowCount() == 2) {
      matrix1 = (Matrix) myTableModel.getValueAt(0, -1);
      matrix2 = (Matrix) myTableModel.getValueAt(1, -1);
      PlotFrame plot = new PlotFrame(matrix1, matrix2);
      plot.setVisible(true);
    } else {
      JOptionPane.showMessageDialog(this, "2 trees are needed");
    }
  }

  private void showTime() {
    Matrix matrix = null;
    boolean pb = false;
    if (getNbRowSelected() == 1) {
      for (int i = 0; i < myTableModel.getRowCount(); i++) {
        if (table.getModel().getValueAt(i, 0).toString().compareTo("true") == 0) {
          if (matrix == null) {
            matrix = (Matrix) myTableModel.getValueAt(i, -1);
          }
        }
      }

      for (int i = 0; i < matrix.getDate().length; i++) {
        if (matrix.getDate()[i].equals("")) {
          JOptionPane.showMessageDialog(this, "Date not set: \n" + matrix.getNames()[i]);
          pb = true;
          break;
        }
      }

      if (!pb) {
        PlotFrame plot = new PlotFrame(matrix);
        plot.setVisible(true);
      }
    } else {
      JOptionPane.showMessageDialog(this, "1 tree is needed");
    }
  }

  // End the application
  private void close() {
    int result = JOptionPane.showConfirmDialog(null, "Are you sure?", "", JOptionPane.YES_NO_OPTION);
    if (result == JOptionPane.YES_OPTION) {
      System.exit(0);
    }
  }

  private void compute() {
    if (motif.getText().compareTo("") != 0) {
      tree = stringToArray(motif.getText(), false);
      String[] columnNames;
      columnNames = new String[nbTip];
      for (int i = 0; i < nbName; i++) {
        columnNames[i] = (String) tempName.elementAt(i);
      }
      treep++;
      Matrix mat = PatristicComputer.getDistances(tree, columnNames, "tree" + treep);
      myTableModel.addRow(mat);


      saveMat.setEnabled(true);
      saveCol.setEnabled(true);
      showTime.setEnabled(true);
      saveDip.setEnabled(true);
      saveR.setEnabled(true);
      saveTime.setEnabled(true);
      delete.setEnabled(true);
      saveStep.setEnabled(true);
    }
  }

  public boolean isApplet() {
    return (applet != null);
  }

  public static void main(String[] args) {
    Patristic patristic = new Patristic(null, GraphicsEnvironment.getLocalGraphicsEnvironment().
            getDefaultScreenDevice().
            getDefaultConfiguration());
  }

  // Load a gif image (used for loading the 16x16 icon gifs)
  public static Image loadImage(String fileName) {
    return Toolkit.getDefaultToolkit().getImage("toolbarButtonGraphics" + File.separator + "general" + File.separator + fileName);
  }

  class TimerListener implements ActionListener {

    ProgressBar progressBar;
    PatristicComputerThread task;
    javax.swing.Timer timer;

    TimerListener(ProgressBar progressBar, PatristicComputerThread task, javax.swing.Timer timer) {
      this.progressBar = progressBar;
      this.task = task;
      this.timer = timer;
    }

    public void actionPerformed(ActionEvent evt) {
      progressBar.progressBar.setValue(task.getCurrent());
      if (task.isDone()) {
        Toolkit.getDefaultToolkit().beep();
        timer.stop();
        task.finish();
        progressBar.setVisible(false);
        saveMat.setEnabled(true);
        saveCol.setEnabled(true);
        showTime.setEnabled(true);
        saveDip.setEnabled(true);
        saveR.setEnabled(true);
        saveTime.setEnabled(true);
        delete.setEnabled(true);
        saveStep.setEnabled(true);
      }
    }
  }
}

