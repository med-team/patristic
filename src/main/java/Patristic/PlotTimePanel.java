/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Patristic;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.text.DecimalFormat;
import java.util.EventObject;
import javax.swing.JPanel;
import org.freehep.graphics2d.VectorGraphics;
/**
 *
 * @author  mateofourbe
 */
public class PlotTimePanel extends JPanel implements OptionListener,Printable{
    private PlotModel plotModel;
    private Matrix list;
    //public PlotList listCoord;
    String infoEvent;
    String infoEvent1;
    String infoEvent2;
    boolean swap=false;
    int dotSize=3;
    
    private static final int XLEFT=100;
    private static final int LINELENGTH=400;
    private static final int WIDTHLINE=1;
    int valueX;
    int valueY;
    double length;
    
    //
    //    BigDecimal maxDistance = 0.0;
    //    int maxTime=0;
    //    int minTime=22220;
    //    int timeRange=0;
    
    String ft="";
    String sd="";
    
    boolean isRegress=false;
    boolean isDiffRatio=false;
    boolean isCaption=true;
    String correlation="";
    
    public PlotTimePanel(PlotModel plotModel) {
        this.plotModel=plotModel;
        infoEvent="";
        infoEvent1="";
        infoEvent2="";
        this.addMouseMotionListener(new mouseMotionHandler());
        addMouseListener(new mouseHandler(this));
        this.addComponentListener(new ComponentListener(){
            public void componentResized(ComponentEvent e){
//                System.out.println("W "+e.getComponent().getWidth());
//                System.out.println("H "+e.getComponent().getHeight());
            }
            public void componentHidden(ComponentEvent e){}
            public void componentShown(ComponentEvent e){}
            public void componentMoved(ComponentEvent e){}
        });
    }
    
    /** Creates a new instance of PlotTime */
    //    public PlotTime(ListDistance2 list) {
    //        this.plotModel=plotModel;
    //        infoEvent="";
    //        infoEvent1="";
    //        infoEvent2="";
    //        this.addMouseMotionListener(new mouseMotionHandler());
    //        addMouseListener(new mouseHandler(this));
    //        this.addComponentListener(new ComponentListener(){
    //            public void componentResized(ComponentEvent e){
    //                System.out.println("W "+e.getComponent().getWidth());
    //                System.out.println("H "+e.getComponent().getHeight());
    //            }
    //            public void componentHidden(ComponentEvent e){}
    //            public void componentShown(ComponentEvent e){}
    //            public void componentMoved(ComponentEvent e){}
    //        });
    //    }
    
    public void highLight(String first, String second){
        ft=first;
        sd=second;
        repaint();
    }
    
    public void paintComponent(Graphics g) {
        if (g == null) return;
        dotSize=Integer.valueOf(plotModel.getDotSize()).intValue();
        VectorGraphics vg = VectorGraphics.create(g);
        drawGraph(vg);
    }
    
    public void drawGraph(VectorGraphics vg) {        
        Dimension dim = getSize();
        Insets insets = getInsets();
        
        vg.setColor(Color.white);
        vg.fillRect(insets.left, insets.top,
        dim.width-insets.left-insets.right,
        dim.height-insets.top-insets.bottom);
        
        
        setBackground(Color.WHITE);
        vg.setColor(Color.BLACK);
        vg.setFont(new Font("Courier New",Font.BOLD,12));
        
        vg.drawLine(XLEFT,XLEFT,XLEFT,XLEFT+LINELENGTH);
        vg.drawLine(XLEFT,XLEFT+LINELENGTH,XLEFT+LINELENGTH,XLEFT+LINELENGTH);
        vg.drawString("Tips: "+infoEvent,10,30);
        //vg.drawString("Correlation r= "+plotModel.getCorrelation().toString(),10,10);
        if(isCaption){
            vg.drawString("Tips: "+infoEvent,10,30);
            vg.drawString("Distance: "+infoEvent1,10,50);
            vg.drawString("Time Difference: "+infoEvent2,10,70);
        }
        vg.drawString("Time Difference",XLEFT-3,XLEFT-4);
        vg.drawString("Patristic distance",XLEFT+LINELENGTH+4,XLEFT+LINELENGTH);
        //Y Scale
        //        vg.drawString(String.valueOf(plotModel.getTimeRange()),XLEFT-40,XLEFT+3);
        //        vg.drawString(String.valueOf(plotModel.getTimeRange()/2),XLEFT-40,XLEFT+(LINELENGTH/2)+3);
        //        vg.drawLine(XLEFT-3, XLEFT, XLEFT, XLEFT);
        //        vg.drawLine(XLEFT-3, XLEFT+(LINELENGTH/2), XLEFT, XLEFT+(LINELENGTH/2));
        // X Scale
        //        vg.drawString(plotModel.getMaxXReal().setScale(2,BigDecimal.ROUND_UP).toString(),XLEFT+LINELENGTH-20,XLEFT+LINELENGTH+15);
        //        vg.drawString(plotModel.getMaxXReal().divide(new BigDecimal("2"),2,BigDecimal.ROUND_UP).toString(),XLEFT+(LINELENGTH/2)-20,XLEFT+LINELENGTH+15);
        //        vg.drawLine(XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH+5);
        //        vg.drawLine(XLEFT+(LINELENGTH/2), XLEFT+LINELENGTH, XLEFT+(LINELENGTH/2), XLEFT+LINELENGTH+5);
        
        
//        // Y Scale
//        BigDecimal xx=0.0;
//        if(plotModel.getYUnit().compareTo(0.0)!=0){
//            while(true){
//                xx=xx.add(plotModel.getYUnit());
//                if(xx.compareTo(plotModel.getMaxYGraph())==0 || xx.compareTo(plotModel.getMaxYGraph())==1)
//                    break;
//                BigDecimal length=new BigDecimal(String.valueOf(LINELENGTH));
//                BigDecimal y=xx.multiply(length);
//                y=y.divide(plotModel.getMaxYGraph(),1);
//                
//                vg.drawLine(XLEFT-3, XLEFT, XLEFT, XLEFT);
//                vg.drawString(plotModel.getMaxYGraph().setScale(2,BigDecimal.ROUND_UP).toString(),XLEFT-40,XLEFT+3);
//                vg.drawLine(XLEFT-3, XLEFT+LINELENGTH-y.intValue(), XLEFT, XLEFT+LINELENGTH-y.intValue());
//                vg.drawString(xx.toString(),XLEFT-40,XLEFT+LINELENGTH-y.intValue());
//            }
//        }
//        else{
//            vg.drawString(plotModel.getMaxYReal().setScale(2,BigDecimal.ROUND_UP).toString(),XLEFT-40,XLEFT+3);
//            vg.drawString(plotModel.getMaxYReal().divide(new BigDecimal(2),2,BigDecimal.ROUND_UP).toString(),XLEFT-40,XLEFT+(LINELENGTH/2)+3);
//            vg.drawLine(XLEFT-3, XLEFT, XLEFT, XLEFT);
//            vg.drawLine(XLEFT-3, XLEFT+(LINELENGTH/2), XLEFT, XLEFT+(LINELENGTH/2));
//        }
//        
//        // X Scale
//        BigDecimal x=0.0;
//        if(plotModel.getXUnit().compareTo(new BigDecimal(0))!=0){
//            while(true){
//                x=x.add(plotModel.getXUnit());
//                if(x.compareTo(plotModel.getMaxXGraph())==0 || x.compareTo(plotModel.getMaxXGraph())==1)
//                    break;
//                BigDecimal length=new BigDecimal(LINELENGTH);
//                BigDecimal y=x.multiply(length);
//                y=y.divide(plotModel.getMaxXGraph(),1);
//                
//                vg.drawLine(XLEFT+y.intValue(), XLEFT+LINELENGTH, XLEFT+y.intValue(), XLEFT+LINELENGTH+5);
//                vg.drawString(x.toString(),XLEFT+y.intValue()-20,XLEFT+LINELENGTH+15);
//                vg.drawString(plotModel.getMaxXGraph().setScale(2,BigDecimal.ROUND_UP).toString(),XLEFT+LINELENGTH-20,XLEFT+LINELENGTH+15);
//                vg.drawLine(XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH+5);
//            }
//        }
//        else{
//            vg.drawString(plotModel.getMaxXReal().setScale(2,BigDecimal.ROUND_UP).toString(),XLEFT+LINELENGTH-20,XLEFT+LINELENGTH+15);
//            vg.drawString(plotModel.getMaxXReal().divide(new BigDecimal(2),2,BigDecimal.ROUND_UP).toString(),XLEFT+(LINELENGTH/2)-20,XLEFT+LINELENGTH+15);
//            vg.drawLine(XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH+5);
//            vg.drawLine(XLEFT+(LINELENGTH/2), XLEFT+LINELENGTH, XLEFT+(LINELENGTH/2), XLEFT+LINELENGTH+5);
//        }
        
        double yAbscissa=0.0;
        double yAbscissaReal=plotModel.getMinYGraph();
        // should be getMaxYReal() not getTheMaxReal() !!!!!!!!!!!!!!!
         String fmt="0.##";
        DecimalFormat df = new DecimalFormat(fmt);
        if(Double.compare(plotModel.getYUnit(),0.0)!=0){
            while(true){
                yAbscissa=yAbscissa + plotModel.getYUnit();
                yAbscissaReal=yAbscissaReal + plotModel.getYUnit();
                if(Double.compare(yAbscissa,plotModel.getMaxYGraph()-plotModel.getMinYGraph())==0 || Double.compare(yAbscissa,plotModel.getMaxYGraph()-plotModel.getMinYGraph())==1)
                    break;
                double y=yAbscissa*LINELENGTH;
                //y=y.divide(plotModel.getMaxYGraph(),1);
                y=y/(plotModel.getMaxYGraph() - plotModel.getMinYGraph());
                vg.drawLine(XLEFT-3, XLEFT+LINELENGTH-(int)y, XLEFT, XLEFT+LINELENGTH-(int)y+(int)plotModel.getMinYGraph());
                vg.drawString(String.valueOf(yAbscissaReal),XLEFT-40,XLEFT+LINELENGTH-(int)y+(int)plotModel.getMinYGraph());
            }
//            vg.drawLine(XLEFT-3, XLEFT, XLEFT, XLEFT);
//                vg.drawString(plotModel.getMaxYGraph().setScale(2,BigDecimal.ROUND_UP).toString(),XLEFT-40,XLEFT+3);
//                vg.drawLine(XLEFT-3, XLEFT+LINELENGTH-y.intValue(), XLEFT, XLEFT+LINELENGTH-y.intValue());
//                vg.drawString(xx.toString(),XLEFT-40,XLEFT+LINELENGTH-y.intValue());
            vg.drawLine(XLEFT-3, XLEFT, XLEFT, XLEFT);
            vg.drawString(df.format(plotModel.getMaxYGraph()),XLEFT-40,XLEFT+3);
        }
        else{
            vg.drawString(df.format(plotModel.getMaxYReal()),XLEFT-40,XLEFT+3);
            vg.drawString(df.format(plotModel.getMaxYReal()/2),XLEFT-40,XLEFT+(LINELENGTH/2)+3);
            vg.drawLine(XLEFT-3, XLEFT, XLEFT, XLEFT);
            vg.drawLine(XLEFT-3, XLEFT+(LINELENGTH/2), XLEFT, XLEFT+(LINELENGTH/2));
        }
        
        double xAbscissa=0.0;
        double xAbscissaReal=plotModel.getMinXGraph();
        // should be getMaxXReal() not getTheMaxReal() !!!!!!!!!!!!!!!
        if(Double.compare(plotModel.getXUnit(),0.0)!=0){
            while(true){
                xAbscissa=xAbscissa + plotModel.getXUnit();
                xAbscissaReal+=plotModel.getXUnit();
                if( Double.compare(xAbscissa,plotModel.getMaxXGraph()-plotModel.getMinXGraph())==0 || Double.compare(xAbscissa,plotModel.getMaxXGraph()-plotModel.getMinXGraph())==1)
                    break;
                double y=xAbscissa*LINELENGTH;
                //y=y.divide(plotModel.getMaxXGraph(),1);
                y=y/(plotModel.getMaxXGraph()-plotModel.getMinXGraph());
                vg.drawLine(XLEFT+(int)y, XLEFT+LINELENGTH, XLEFT+(int)y, XLEFT+LINELENGTH+5);
                vg.drawString(String.valueOf(xAbscissaReal),XLEFT+(int)y-20,XLEFT+LINELENGTH+15);
            }
            vg.drawString(df.format(plotModel.getMaxXGraph()-plotModel.getMinXGraph()),XLEFT+LINELENGTH-20,XLEFT+LINELENGTH+15);
            vg.drawLine(XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH+5);
        }
        else{
            vg.drawString(df.format(plotModel.getMaxXReal()/2),XLEFT+(LINELENGTH/2)-20,XLEFT+LINELENGTH+15);
            vg.drawString(df.format(plotModel.getMaxXReal()),XLEFT+LINELENGTH-20,XLEFT+LINELENGTH+15);
            vg.drawLine(XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH+5);
            vg.drawLine(XLEFT+(LINELENGTH/2), XLEFT+LINELENGTH, XLEFT+(LINELENGTH/2), XLEFT+LINELENGTH+5);
        }
        
        vg.drawString("0",XLEFT,XLEFT+LINELENGTH+15);
        drawDots(vg);
        
        //g.drawLine(XLEFT,XLEFT+LINELENGTH-intercept.intValue(), XLEFT+LINELENGTH,XLEFT+LINELENGTH-(slope.multiply(new BigDecimal(LINELENGTH)).add(intercept)).intValue());
    }
    
    // Draw one line: match line or mismatch line with their positions
    private void drawDots(VectorGraphics vg){
        int twoI=0;
        int oneI=0;
        int highLightx=-1;
        int highLighty=-1;
        PlotList plotList=plotModel.getPlotList();
        //if (list.getList().isEmpty()==false) {
        for(int i=0;i<plotList.getNbPosition();i++){
            if(swap){
                twoI=plotList.getList().elementAt(i).getX();
                oneI=plotList.getList().elementAt(i).getY();
            }
            else{
                oneI=plotList.getList().elementAt(i).getX();
                twoI=plotList.getList().elementAt(i).getY();
            }
            if(oneI==-1 || twoI==-1) continue;
           if(Double.compare(plotList.getList().elementAt(i).getXx(),plotModel.getMaxXGraph())==1
            || Double.compare(plotList.getList().elementAt(i).getYy(),plotModel.getMaxYGraph())==1) continue;
            
            if( plotList.getList().elementAt(i).getName1().compareTo(ft)==0 && plotList.getList().elementAt(i).getName2().compareTo(sd)==0 ||
                    plotList.getList().elementAt(i).getName1().compareTo(sd)==0 && plotList.getList().elementAt(i).getName2().compareTo(ft)==0){vg.setColor(Color.RED);}
//            else if(((PlotCoord)plotList.getList().elementAt(i)).isDiff() && isDiffRatio){
//                vg.setColor(Color.BLUE);
//            }
            else vg.setColor(Color.BLACK);
            vg.fillRect(XLEFT+oneI,XLEFT+LINELENGTH-twoI,dotSize,dotSize);
        }
        //}
    }
    
    public void plotChanged(EventObject e) {
        repaint();
    }
    
    public void swap(){
        if(swap)swap=false;
        else swap=true;
        repaint();
    }
    
    public int print(Graphics g, PageFormat pageFormat, int pi) throws PrinterException {
        if (pi >= 1) {
            return Printable.NO_SUCH_PAGE;
        }
        VectorGraphics vg = VectorGraphics.create(g);
        drawGraph(vg);
        return Printable.PAGE_EXISTS;
    }
    
    
    /**
     * Class used for the mouseMoved method, which is used for getting the coordinates
     * of the mouse and therefore to get information of a match or a mismatch
     */
    //    class mouseMotionHandler extends MouseMotionAdapter {
    //        public void mouseMoved(MouseEvent e) {
    //            valueX=e.getX();
    //            valueY=e.getY();
    //
    //                for(int i=0;i<listCoord.getNbPosition();i++){
    //                int oneI=((PlotCoord)listCoord.getList().elementAt(i)).getX()+XLEFT;
    //                int twoI=XLEFT+LINELENGTH-((PlotCoord)listCoord.getList().elementAt(i)).getY();
    //
    //                if ((valueX > oneI && valueX<=oneI+dotSize)&&(valueY<=twoI+dotSize && valueY > twoI) ) {
    //                       infoEvent=((PlotCoord)listCoord.getList().elementAt(i)).getName1()+" & "+((PlotCoord)listCoord.getList().elementAt(i)).getName2();
    //                       infoEvent1=((PlotCoord)listCoord.getList().elementAt(i)).getXx().toString();
    //                       infoEvent2=String.valueOf(((PlotCoord)listCoord.getList().elementAt(i)).getTimeY());
    //                       repaint();
    //                       break;
    //                    }
    //                else{
    //                    infoEvent="";
    //                    infoEvent1="";
    //                    infoEvent2="";
    //                }
    //            }
    //        }
    //    }
    
    
    class mouseMotionHandler extends MouseMotionAdapter {
        public void mouseMoved(MouseEvent e) {
            PlotList plotList=plotModel.getPlotList();
            valueX=e.getX();
            valueY=e.getY();
            int oneI;
            int twoI;
            for(int i=0;i<plotList.getNbPosition();i++){
                if(swap){
                    twoI=XLEFT+LINELENGTH-plotList.getList().elementAt(i).getX();
                    oneI=plotList.getList().elementAt(i).getY()+XLEFT;
                    //oneI=((PlotCoord)plotList.getList().elementAt(i)).getY()+XLEFT;
                    //twoI=XLEFT+LINELENGTH-((PlotCoord)plotList.getList().elementAt(i)).getX();
                }
                else{
                    twoI=XLEFT+LINELENGTH-plotList.getList().elementAt(i).getY();
                    oneI=plotList.getList().elementAt(i).getX()+XLEFT;
                    //oneI=((PlotCoord)plotList.getList().elementAt(i)).getX()+XLEFT;
                    //twoI=XLEFT+LINELENGTH-((PlotCoord)plotList.getList().elementAt(i)).getY();
                }
                
                if ((valueX > oneI && valueX<=oneI+dotSize)&&(valueY<=twoI+dotSize && valueY > twoI) ) {
                    infoEvent=plotList.getList().elementAt(i).getName1()+" & "+plotList.getList().elementAt(i).getName2();
                    infoEvent1=String.valueOf(plotList.getList().elementAt(i).getXx());
                    infoEvent2=String.valueOf(plotList.getList().elementAt(i).getYy());
                    repaint();
                    break;
                }
                else{
                    infoEvent="";
                    infoEvent1="";
                    infoEvent2="";
                }
            }
        }
    }
    
    private class mouseHandler extends java.awt.event.MouseAdapter {
        PlotTimePanel panel;
        mouseHandler(PlotTimePanel panel){
            this.panel=panel;
        }
        public void mouseClicked(MouseEvent e) {
            panel.swap();
            //        System.out.println("H: "+panel.getHeight());
            //        System.out.println("W: "+panel.getWidth());
        }
    }
}
