/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Patristic;

import javax.swing.*;
//import javax.swing.event.*;
//import javax.swing.text.*;
//import javax.swing.border.*;
//import javax.swing.colorchooser.*;
//import javax.swing.filechooser.*;
//import javax.accessibility.*;

import java.awt.*;
//import java.awt.event.*;
//import java.beans.*;
//import java.util.*;
//import java.io.*;
//import java.applet.*;
//import java.net.*;

public class PatristicApplet extends JApplet {
    public void init() {
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(new Patristic(this), BorderLayout.CENTER);
    }

//    public URL getURL(String filename) {
//        URL codeBase = this.getCodeBase();
//        URL url = null;
//	
//        try {
//            url = new URL(codeBase, filename);
//	    System.out.println(url);
//        } catch (java.net.MalformedURLException e) {
//            System.out.println("Error: badly specified URL");
//            return null;
//        }
//
//        return url;
//    }


}
