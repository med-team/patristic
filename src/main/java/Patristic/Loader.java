/*
 * Loader.java
 *
 * Created on 3 August 2004, 10:58
 */
/**
 * load a matrix from a PAUP file
 * @author  formmat
 */
package Patristic;

import java.io.*;
import javax.swing.*;
import java.util.*;

/**
 * Open a file and create an header and keep only the sequence
 */
public class Loader {

  public static int UPPERMATRIX = 0;
  public static int LOWERMATRIX = 1;
  public static int COLUMN = 2;
  public static int MEGA = 3;
  public static int PHYLIP = 4;
  public static int DIPLOMO = 5;
  private int prog;
  private String fileName;
  private Matrix list;
  private int nbTaxa = 0;
  String[] ColNames;
  double sbl = 0;

  public Loader(String path, int prog) {
    try {
      BufferedReader in = new BufferedReader(new FileReader(path));
      String fileSeparator = System.getProperty("file.separator");
      fileName = path.substring(path.lastIndexOf(fileSeparator) + 1);
      switch (prog) {
        case 0:
          upperMatrix(toArray(in));
          break;
        case 1:
          lowerMatrix(toArray(in));
          break;
        case 2:
          column(in);
          break;
        case 3:
          mega(in);
          break;
        case 4:
          phylip(in);
          break;
        case 5:
          diplomo(in);
          break;
        default:
          break;
      }
      in.close();
    } catch (IOException err) {
      System.out.println("IO " + err.getMessage());
    } catch (NullPointerException err) {
      err.printStackTrace();
    }
  }

  public Matrix getList() {
    return list;
  }

  // Load matrix (PAUP)
  private void lowerMatrix(String[][] array) {
    int nb = 1;
    list = new Matrix(ColNames, fileName);
    double[][] matrix = new double[nbTaxa][nbTaxa];
    for (int i = 0; i < nbTaxa; i++) {
      for (int j = 0; j < nbTaxa; j++) {
        matrix[i][j] = 0;
      }
    }
    for (int j = 1; j <= nbTaxa; j++) {
      for (int i = nb; i <= nbTaxa; i++) {
        sbl = sbl + Double.valueOf(array[j][i]);
        matrix[j - 1][i - 1] = Double.valueOf(array[j][i]);
        matrix[i - 1][j - 1] = Double.valueOf(array[j][i]);
        if (nb > nbTaxa) {
          break;
        }
      }
      nb++;
    }
    list.setSbl(sbl);
    list.setMatrix(matrix);
  }

  private void upperMatrix(String[][] array) {
    int nb = 1;
    list = new Matrix(ColNames, fileName);
    double[][] matrix = new double[nbTaxa][nbTaxa];
    for (int i = 0; i < nbTaxa; i++) {
      for (int j = 0; j < nbTaxa; j++) {
        matrix[i][j] = 0;
      }
    }
    for (int i = 1; i <= nbTaxa; i++) {
      for (int j = 1; j <= nbTaxa; j++) {
        sbl = sbl + Double.valueOf(array[j][i]);
        matrix[j - 1][i - 1] = Double.valueOf(array[j][i]);
        matrix[i - 1][j - 1] = Double.valueOf(array[j][i]);
        if (nb > nbTaxa) {
          break;
        }
      }
      nb++;
    }
    list.setSbl(sbl);
    list.setMatrix(matrix);
  }

  private String[][] toArray(BufferedReader in) throws IOException {
    boolean eof = false;
    boolean isBegin = true;
    int posj = 0;
    nbTaxa = Integer.parseInt(in.readLine());
    String[][] array = new String[nbTaxa + 1][nbTaxa + 1];
    ColNames = new String[nbTaxa];
    int posi = 0;
    int margin = 0;
    String[] names = null;
    while (!eof) {

      String line = in.readLine();
      if (line == null) {
        eof = true;
      } else {
        if (isBegin) {
          names = line.split("\\s+");
          array[0][0] = "";
          for (int i = 1; i < names.length; i++) {
            array[i + posi][0] = names[i];
            ColNames[i + posi - 1] = names[i];
          }
          isBegin = false;
          while (line.substring(margin, margin + 1).compareTo(" ") == 0) {
            margin++;
          }
        } else if (line.compareTo("") == 0) {
          isBegin = true;
          posj = 0;
          posi += names.length - 1;
        } else {
          posj++;
          String tempp = line.substring(margin - 2);
          String[] temp = tempp.split("\\s+");
          array[0][posj] = line.substring(0, margin - 1);
          for (int i = 1; i < temp.length; i++) {
            if (temp[i].compareTo("-") == 0) {
              temp[i] = "0";
            }
            array[i + posi][posj] = temp[i];
          }
        }
      }
    }
    return array;
  }

  // Load Column (PAUP)
  private void column(BufferedReader in) throws IOException {
    int nbPair = 0;
    Vector vec = new Vector();
    Vector vecName = new Vector();
    boolean eof = false;
    while (!eof) {
      String line = in.readLine();
      if (line == null) {
        eof = true;
      } else {
        String[] temp = line.split("\\t+");
        vec.add(temp[2]);
        if (nbPair == 0) {
          vecName.add(temp[1]);
        }
        if (!vecName.contains(temp[0])) {
          vecName.add(temp[0]);
        }
        nbPair++;
      }
    }
    nbPair = 0;
    int nbTip = 1;
    double[][] matrix = new double[vecName.size()][vecName.size()];
    ColNames = new String[vecName.size()];
    for (int i = 0; i < vecName.size(); i++) {
      ColNames[i] = vecName.elementAt(i).toString();
    }
    list = new Matrix(ColNames, fileName);
    for (int i = 0; i < vecName.size(); i++) {
      for (int j = 0; j < vecName.size(); j++) {
        matrix[i][j] = 0;
      }
    }

    while (true) {
      if (vec.size() <= nbPair) {
        break;
      }
      for (int k = 0; k < nbTip; k++) {
        sbl = sbl+Double.valueOf(vec.elementAt(nbPair).toString());
        matrix[nbTip][k] = Double.valueOf(vec.elementAt(nbPair).toString());
        matrix[k][nbTip] = Double.valueOf(vec.elementAt(nbPair).toString());
        nbPair++;
      }
      nbTip++;
    }
    list.setMatrix(matrix);
    list.setSbl(sbl);
  }

  private void mega(BufferedReader in) throws IOException {
    boolean eof = false;
    boolean isBegin = false;
    boolean header = false;
    boolean saut = false;
    String[] names = null;
    Vector colNames = new Vector();
    int nbColNames = 0;
    double[][] matrix = null;
    int lineNb = 0;

    java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("#");
    java.util.regex.Matcher matcher = pattern.matcher("");
    String line;
    while ((line = in.readLine()) != null) {
      matcher.reset(line);
      if (isBegin) {
        names = line.substring(line.indexOf("]") + 1).split("\\s+");
        if (names[names.length - 1].compareTo("") == 0) {
          break;
        }
        for (int i = 1; i < names.length; i++) {
          sbl = sbl+Double.valueOf(names[i]);
          matrix[lineNb][lineNb + i] = Double.valueOf(names[i]);
          matrix[lineNb + i][lineNb] = Double.valueOf(names[i]);
        }
        lineNb++;
      } else if (matcher.find() && line.compareTo("#mega") != 0) {
        //                if(newF){
        colNames.add(line.substring(line.indexOf("#") + 1));
        nbColNames++;
        //                }
        header = true;
      } else if (header && line.compareTo("") == 0) {
        //                if(newF)str+="\r";
        //                str+="title\r";
        saut = true;
      } else if (saut) {
        isBegin = true;
        saut = false;
        String[] columnNames = new String[nbColNames];
        for (int i = 0; i < nbColNames; i++) {
          columnNames[i] = (String) colNames.elementAt(i);
        }
        list = new Matrix(columnNames, fileName);
        matrix = new double[nbColNames][nbColNames];
        for (int i = 0; i < nbColNames; i++) {
          for (int j = 0; j < nbColNames; j++) {
            matrix[i][j] = 0;
          }
        }
      }


    }
    list.setMatrix(matrix);
    list.setSbl(sbl);
  }

  private void phylip(BufferedReader in) throws IOException {
    boolean eof = false;
    Vector colNames = new Vector();
    ;

    int lineNb = 0;
    int nb = 0;
    boolean lineFinished = true;

    int nbTip = Integer.valueOf(in.readLine().replaceAll(" ", "")).intValue();
    double[][] matrix = new double[nbTip][nbTip];
    String[] columnNames = new String[nbTip];
    String line;
    while ((line = in.readLine()) != null) {
      if (lineNb > nbTip) {
        break;
      }
      String[] lineArray = line.split("\\s{2}");
      //System.out.println(lineArray.length);
      if (lineFinished) {
        for (int i = 1; i < lineArray.length; i++) {
          columnNames[lineNb] = lineArray[0];
          matrix[lineNb][i - 1] = Double.valueOf(lineArray[i]);
          sbl = sbl+Double.valueOf(lineArray[i]);
        }
        nb += lineArray.length - 1;
        if (nb < nbTip) {
          lineFinished = false;
        } else {
          lineNb++;
          lineFinished = true;
        }
      } else {
        for (int i = 1; i < lineArray.length; i++) {
          System.out.println(lineArray[0]);
          //System.out.println(lineArray[1]);
          matrix[lineNb][i - 1 + nb] = Double.valueOf(lineArray[i]);
          sbl = sbl + Double.valueOf(lineArray[i]);
        }
        nb += lineArray.length;
        if (nb < nbTip) {
          lineFinished = false;
        } else {
          lineNb++;
          lineFinished = true;
          nb = 0;
        }
      }
    }
    list = new Matrix(columnNames, fileName);
    list.setMatrix(matrix);
    list.setSbl(sbl);
  }

  private void diplomo(BufferedReader in) throws IOException {
    boolean isMatrix = false;
    boolean isHeader = false;
    boolean saut = false;
    String[] distances = null;
    Vector colNames = new Vector();
    int nbColNames = 0;
    int lineNb = 0;
    double[][] matrix = null;
    String line = "";
    while ((line = in.readLine()) != null) {
      if (isMatrix) {
        distances = line.split("\\s+");
        for (int i = 1; i < distances.length; i++) {
          sbl = sbl+Double.valueOf(distances[i]);
          matrix[lineNb][lineNb + i] = Double.valueOf(distances[i]);
          matrix[lineNb + i][lineNb] = Double.valueOf(distances[i]);
        }
        lineNb++;

      } else if (!saut && line.compareTo("") != 0) {
        colNames.add(line);
        nbColNames++;
        isHeader = true;
      } else if (isHeader && line.compareTo("") == 0) {
        saut = true;
      } else if (saut) {
        isMatrix = true;
        saut = false;
        String[] columnNames = new String[nbColNames];
        for (int i = 0; i < nbColNames; i++) {
          columnNames[i] = (String) colNames.elementAt(i);
        }
        list = new Matrix(columnNames, fileName);
        matrix = new double[nbColNames][nbColNames];
        for (int i = 0; i < nbColNames; i++) {
          for (int j = 0; j < nbColNames; j++) {
            matrix[i][j] = 0;
          }
        }
      }
    }
    list.setMatrix(matrix);
    list.setSbl(sbl);
  }
}