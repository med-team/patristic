/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 *
 * @author  Fourment Mathieu
 */
package Patristic;

import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;

/**
 * GUI used for the graphic display of the results
 */
public class FrameMatrix extends JFrame {

  JTable table;
  MatrixTableModel model;
  int rowFrom;

  /** Creates a new instance of Diagram */
  public FrameMatrix(Matrix matrix) {
    setSize(600, 400);
    this.setResizable(true);
    String sbl="";
    if(matrix.getSBL()!=-999) sbl=" (SBL:"+String.valueOf(matrix.getSBL())+ ")";
    setTitle("Patristic distances: " + matrix.getFileName() + sbl);
    model = new MatrixTableModel(matrix);
    table = new JTable(model);
    table.getTableHeader().setReorderingAllowed(false);
    table.addMouseListener(new java.awt.event.MouseAdapter() {

      @Override
      public void mousePressed(MouseEvent evt) {
        if (evt.getButton() == 1) {
          rowFrom = table.rowAtPoint(evt.getPoint());
        } else if (evt.getButton() == 3) {
          //deleteRow(evt);
          //deleteColumn(evt);
        }
      }

      @Override
      public void mouseReleased(MouseEvent evt) {
        if (evt.getButton() == 1) {
          tableMouseReleased(evt);
        }
      }
    });

    table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    JScrollPane scroll = new JScrollPane(table);
    add(scroll);
  }

  

  private void tableMouseReleased(MouseEvent evt) {
    int rowTo = table.rowAtPoint(evt.getPoint());
    if (rowTo != rowFrom && rowTo > -1 && rowTo < table.getRowCount()) {
      model.move(rowFrom, rowTo);
      model.fireTableDataChanged();
      model.fireTableStructureChanged(); // to update the header
    }
  }

  

  class MatrixTableModel extends AbstractTableModel {

    private Matrix matrix;

    public MatrixTableModel(Matrix matrix) {
      this.matrix = matrix;
    }

    public int getRowCount() {
      return matrix.getNames().length;
    }

    public int getColumnCount() {
      return matrix.getNames().length + 1;
    }

    @Override
    public String getColumnName(int col) {
      if (col == 0) {
        return "";
      }
      return matrix.getNames()[col - 1];
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
      if (columnIndex == 0) {
        return matrix.getNames()[rowIndex];
      }
      return matrix.getMatrix()[rowIndex][columnIndex - 1];
    }

    public void move(int from, int to) {
      if (from == to) {
        return;
      }
      matrix.move(from, to);
    }


}

//  private void changeMatrix() {
//    localMatrix.setMatrix(toSimpleMatrix());
//  }
//
//
//  private void deleteRow(MouseEvent evt) {
//    int row = table.rowAtPoint(evt.getPoint());
//    if (time.size() != 0) {
//      time.remove(row); // time array
//    }
//    ((MyDefaultTableModel) table.getModel()).removeRow(row);
//    ((MyDefaultTableModel) table.getModel()).fireTableRowsDeleted(row, row);
//  }
//
//  private void deleteColumn(MouseEvent evt) {
//    int row = table.rowAtPoint(evt.getPoint());
//    //table.removeColumn(table.getColumnModel().getColumn(row));
//
////        // Disable autoCreateColumnsFromModel to prevent
////        // the reappearance of columns that have been removed but
////        // whose data is still in the table model
////        table.setAutoCreateColumnsFromModel(false);
////
////        // Remove the first visible column and its data
//    removeColumnAndData(table, row);
//  }
//
//  public void removeColumnAndData(JTable table, int vColIndex) {
//    MyDefaultTableModel model = (MyDefaultTableModel) table.getModel();
//    TableColumn col = table.getColumnModel().getColumn(vColIndex + 1);
//    int columnModelIndex = col.getModelIndex();
//    Vector data = model.getDataVector();
//    Vector colIds = model.getColumnIdentifiers();
//
//    // Remove the column from the table
//    table.removeColumn(col);
//
//    // Remove the column header from the table model
//    colIds.removeElementAt(columnModelIndex);
//
//    // Remove the column data
//    for (int r = 0; r < data.size(); r++) {
//      Vector row = (Vector) data.get(r);
//      row.removeElementAt(columnModelIndex);
//    }
//    model.setDataVector(data, colIds);
//
//    // Correct the model indices in the TableColumn objects
//    // by decrementing those indices that follow the deleted column
//    Enumeration enume = table.getColumnModel().getColumns();
//    for (; enume.hasMoreElements();) {
//      TableColumn c = (TableColumn) enume.nextElement();
//      if (c.getModelIndex() >= columnModelIndex) {
//        c.setModelIndex(c.getModelIndex() - 1);
//      }
//    }
//    model.fireTableStructureChanged();
//  }
}
