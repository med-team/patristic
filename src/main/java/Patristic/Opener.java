/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author  Fourment Mathieu
 */
package Patristic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Open a file and create an header and keep only the sequence
 */
public class Opener{
    private String path;
    private String tree="";
    private String line;
    private int length;
    private Vector array;
    private String[] name;
    private Vector treeList=new Vector();
    
    boolean nexus=false;
    final String SPACE = "[ \t\f\n\r]+";
    //final String SPACES = "[ \t\f\n\r]*";
    
    //    Pattern pattern = Pattern.compile("^tree");
    //    Matcher matcher = pattern.matcher("");
    
    Pattern pattern = Pattern.compile("tree");
    Matcher matcher = pattern.matcher("");
    //    Pattern pBracket = Pattern.compile("^[");
    //    Matcher mBracket = pBracket.matcher("");
    
    Pattern pattern1 = Pattern.compile("=");
    Matcher matcher1 = pattern1.matcher("");
    
    /** Creates a new instance of Opener */
    public Opener(String path) {
        this.path=path;
    }
    
    public boolean isNexus(){
        return nexus;
    }
    // Carry on the action
    public String getText(){
        try{
            tree="";
            BufferedReader in = new BufferedReader(new FileReader(path));
            line=in.readLine();
            if(line.compareTo("(")==0){
                loadPh(in);
            }
            else if(line.substring(0,1).compareTo("[")==0){
                line=line.substring(line.indexOf("]")+1,line.length());
                loadPh(in);
            }
            else
                if(line.startsWith("#NEXUS")){
                    nexus=true;
                    loadNexus(in);
                }
                else{
                    loadPh(in);
                }
            in.close();
        }
        catch (IOException err) {System.out.println("IO "+err.getMessage());}
        catch (NullPointerException err) {err.printStackTrace();}
        
        return tree;
    }
    
    private void loadPh(BufferedReader in) throws IOException{
        tree+=line;
//        while((line=in.readLine())!=null){
//            text+=line;
//        }
    }
    
    // Load a Nexus
    private void loadNexus(BufferedReader in) throws IOException{
        boolean translateFlag=false;
        boolean treeBlock = false;
        array=new Vector();
        
        while((line=in.readLine())!=null){
            String lline=line.toLowerCase();
            if( lline.matches("\\s*begin\\s+trees;.*")){
                treeBlock = true;
            }
            else if(treeBlock && lline.matches("\\s*end;\\s*")){
                treeBlock = false;
            }
            else if( treeBlock ){
                if(lline.matches("\\s*translate\\s*")){
                    translateFlag=true;
                }
                else if(translateFlag){
                    Pattern p = Pattern.compile("\\s*(\\d+)\\s+(\\S.*)");
                    Matcher m = p.matcher(line);
                    if(m.find()){
                        length++;
                        String str = m.group(2).trim();
                        if(str.endsWith(";")){System.out.println(str); translateFlag = false;}
                        if( str.endsWith(",") || str.endsWith(";") )
                            array.add(str.substring(0,str.length()-1));
                        else array.add(str);
                    }
                     else{
                        translateFlag = false;
                     }
                 
                }
                 else if( lline.matches("\\s*u?tree\\s+.+") ){
                     tree = line.split("=")[1];
                    int start = 0;
                    int end = tree.length()-1;
                    while(tree.charAt(start) != '('){start++;}
                    while(tree.charAt(end)   != ')'){end--;}
                    tree = tree.substring(start, end+1) + ";";
                    treeList.add(tree);
                 }

            }

            /*if (matcher.find() && matcher1.find() && treeFlag) {
                int pos=0;
                String charr="z";
                while(charr.compareTo("(")!=0){
                    charr=line.substring(pos,pos+1);
                    pos++;
                }
                pos--;
                text = line.substring(pos,line.length());
                treeList.add(text);
                System.out.println(text);
                //eof=true;
            }
            
            else if(line.matches("\t*;") && translateFlag==true){
                translateFlag=false;
                treeFlag=true;
            }*/
        }
    }
    
    public String[] getName(int length){
        if(nexus){
            name=new String[length];
            for(int i=0;i<length;i++) name[i]= (String)array.elementAt(i);
        }
        else{
            name=new String[length];
            for(int i=0;i<length;i++) name[i]= String.valueOf(i+1);
        }
        return name;
    }
    
    public Vector getTreeList(){
        return treeList;
    }
    
}
