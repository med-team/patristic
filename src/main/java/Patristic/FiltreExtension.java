/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 *
 * @author  Fourment Mathieu
 */
package Patristic;

import java.io.File;

/**
 * Class used to have personalised extension filters
 */
public class FiltreExtension extends javax.swing.filechooser.FileFilter {

    final private String extension;
    final private String description;

    /** Creates a new instance of FiltreExtension */
    public FiltreExtension(String extension, String description) {
        if (extension.indexOf('.') == -1)
            extension = "." + extension;
        this.extension = extension;
        this.description = description;
    }

    // Use the filter on the files but keep the directories
    public boolean accept(File file) {
        if (file.isDirectory()) return true;

        if (file.getName().toLowerCase().endsWith(extension)) return true;
        return false;
    }

    // return the filter description
    public String getDescription() {
        return this.description + "(*" + extension + ")";
    }
}
