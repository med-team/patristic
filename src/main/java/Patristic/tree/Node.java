/*
 * Node.java
 *
 * Created on July 31, 2007, 12:05 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Patristic.tree;

import java.util.Vector;

/**
 *
 * @author mathieu
 */
public  class Node {
    
    private Vector<Node> children=new Vector<Node>();
    private Node parent=null;
    private String name=null;
    private double brlens=0; // branch length leading to itself
    private double bootstrap=0;
    
    /** Creates a new instance of Node */
    public Node(String name) {
        this.name=name;
    }
    
    
    /** Creates a new instance of Node */
    public Node() {
        this.name=null;
    }
    
    
    @Override
    public String toString(){
        return name;
    }
    
//    @Override
//    public boolean equals(Object node){
//        return (((Node) node).name.equals(this.name)? true: false);
//    }
    
    
    public Node getParent(){
        return parent;
    }
    
    public String getName(){
        return name;
    }
    
    public Vector<Node> getChildren(){
        return children;
    }
    
    public double getBrlens(){
        return brlens;
    }
    
    public double getBootstrap(){
        return bootstrap;
    }
    
    
    public void setParent(Node parent){
        this.parent=parent;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setChildren(Vector<Node> children){
        this.children = children;
    }
    
    public void addChild(Node child){
        children.addElement(child);
    }
    
    public void setBrlens(double brlens){
        this.brlens=brlens;
    }
    
    public void setBootstrap(double bootstrap){
        this.bootstrap=bootstrap;
    }
    
    public void removeChild(Node child){
        for(int i=0;i<children.size();i++){
            if(children.elementAt(i) == child){
                Node removed=children.remove(i);
                removed.setParent(null);
                break;
            }
        }
    }
    
    public boolean isRoot(){
        return (parent==null);
    }
    
    public boolean isLeaf(){
        return (children.size() == 0);
    }
}
