/*
 * Tree.java
 *
 * Created on July 11, 2007, 3:52 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Patristic.tree;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

/**
 *
 * @author mathieu
 */
public class Tree{
    private String tree="default";
    public double depth=-1;
    public int length=-1;
    public boolean isBootstrap=false;
    public boolean isBrlens=false;
    private Node root=null;
    private Vector<Node> refTaxa=new Vector<Node>();
    private Hashtable<String, Node> nameLookup = new Hashtable<String,Node>();
    
    /** Creates a new instance of Tree */
    public Tree(String t) {
        tree=t;
    }
    
    public Tree(){
        tree="default";
    }
    
    public void setTree(Node node){
        root=node;
    }
    public void setTaxa(Vector<Node> vec){
        refTaxa=vec;
    }
    
    public void setNameLookup(Hashtable<String, Node> lookup){
        this.nameLookup=lookup;
    }
    
    public Node getRoot(){
        return root;
    }
    
    public void setRoot(Node r){
        root=r;
    }
    
    public double getDepth(){
        if(depth == -1) return getDepth(root);
        return depth;
    }
    
    public int getLength(){
        if(length == -1) return getLength(root);
        return length;
    }
    
    
    
    public int getLength(Node tree){
        int depth=0;
        int max=0;
        max=getLengthAux(tree,depth,max);
        return max;
    }
    
    private int getLengthAux(Node tree,int depth, int max){
        if(tree!=null){
            depth+=1;
            if(tree.getName()!=null & depth>max) max=depth; //PRE
            //System.err.println("PRE "+tree.name);
            for(Iterator<Node> it=tree.getChildren().iterator();it.hasNext();){
                max=getLengthAux(it.next(),depth,max);
                //System.err.println("INTER "+tree.name);
            }
            //System.err.println("POST "+tree.name);
            depth-=1; //POST
        }
        return max;
    }
    
    public double getDepth(Node tree){
        double depth=0;
        double max=0;
        max=getDepthAux(tree,depth,max);
        return max;
    }
    
    private double getDepthAux(Node tree, double depth, double max){
        if(tree!=null){
            depth+=tree.getBrlens();
            if(tree.getName()!=null & depth>max) max=depth; //PRE
            //System.err.println("PRE "+tree.name);
            for(Iterator<Node> it=tree.getChildren().iterator();it.hasNext();){
                max=getDepthAux(it.next(),depth,max);
                //System.err.println("INTER "+tree.name);
            }
            //System.err.println("POST "+tree.name);
            depth-=tree.getBrlens(); //POST
        }
        return max;
    }
    
    public String getNewick() {
        StringBuffer newick=new StringBuffer();
        getNewick(root,newick);
        return newick.toString();
    }
    
    private void getNewick(Node tree, StringBuffer newick) {
        if(tree==null)return;
        if(tree.getName() != null){
            newick.append(tree.getName()).append(":").append(tree.getBrlens());
            return;
        } else newick.append("(");
        for(Iterator<Node> it=tree.getChildren().iterator();it.hasNext();){
            getNewick(it.next(),newick);
            if(it.hasNext())newick.append(",");
        }
        newick.append("):").append(tree.getBrlens());
    }
    
    
    public Vector<Node> getTaxa(){
        return refTaxa;
    }
    
    public Hashtable<String, Node> getNameLookup(){
        return this.nameLookup;
    }
    
    public void reroot(Node node){
        if(node.getParent().isRoot()) return;
        
        reroot(node.getParent());
        
//        // in order to get a rooted tree
//        if(node.getParent().getChildren().size() > 2){
//            DrawableNode newRoot = new DrawableNode();
//            newRoot.addChild(node);
//            newRoot.addChild(node.getParent());
//            node.getParent().removeChild(node);
//            node.setParent(newRoot);
//            tree.setRoot(newRoot);
//        }
//        else 
            setRoot(node.getParent());
    }
    
    private void reroot_aux(Node node){
        if (node.isRoot() || node.isLeaf()) return;
        
        if (!node.getParent().isRoot()){
            reroot_aux(node.getParent());
        }
        
        double brlens = node.getParent().getBrlens();
        node.getParent().setBrlens(node.getBrlens());
        node.setBrlens(brlens);
        
        double bootstrap = node.getParent().getBootstrap();
        node.getParent().setBootstrap(node.getBootstrap());
        node.setBootstrap(bootstrap);
        
        
        Node parent = node.getParent();
        parent.removeChild(node);
        node.addChild(parent);
        parent.setParent(node);
    }
}
