/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Patristic;

import java.util.EventListener;
import java.util.EventObject;
import java.util.Stack;
import javax.swing.JOptionPane;
import javax.swing.event.EventListenerList;
/**
 *
 * @author  mateofourbe
 */
public class PlotModel{
    private double maxXGraph,maxXReal,maxYGraph,maxYReal;
    private double theMax;
    private double minXGraph,minXReal,minYGraph,minYReal;
    private double unitX,unitY;
    private double minNotZeroX,minNotZeroY;
    private double rateMutation;
    private double invRate;
    private double correlation;
    private String formula;
    private double std;
    private String dotSize;
    private Matrix list1;
    private Matrix list2;
    private PlotList plotList;
    private static final int LINELENGTH=400;
    private static final double LINELENGTH_BIG=LINELENGTH;
    private boolean isRegress=false;
    private boolean isCaption=false;
    private  EventListenerList listenerList = new EventListenerList();

    double maxTime=0.0;
    double minTime=22222220;
    double timeRange=0.0;

    double[] slope;
    double[] intercept;
    double meanStat;
    double xStd;

    double X_LEFT_OFFSET = 100;
    double X_RIGHT_OFFSET = 100;
    double Y_UP_OFFSET = 100;
    double Y_DOWN_OFFSET = 100;
    double X_AXIS_LENGTH = 400;
    double Y_AXIS_LENGTH = 400; // should depend on the frame size or maybe something else if we zoom

    /** Creates a new instance of OptionObject */
    public PlotModel(Matrix list1,Matrix list2) {
        this.list1=list1;
        this.list2=list2;
        init();
    }

    /** Creates a new instance of OptionObject */
    public PlotModel(Matrix list1) {
        this.list1=list1;
        this.list2=null;
        initTime();
    }

    public PlotList getPlotList(){
        return plotList;
    }

    public Matrix getList1(){
        return list1;
    }

    public Matrix getList2(){
        return list2;
    }

    public double getCorrelation(){
        return correlation;
    }

    public double getMaxXReal(){
        return maxXReal;
    }
    public double getMaxYReal(){
        return maxYReal;
    }

    public double getMaxTime(){
        return maxTime;
    }

    public double getTimeRange(){
        return timeRange;
    }

    public double getMaxXGraph(){
        return maxXGraph;
    }
    public double getMaxYGraph(){
        return maxYGraph;
    }
    public double getMinXReal(){
        return minXReal;
    }
    public double getMinYReal(){
        return minYReal;
    }
    public double getTheMaxReal(){
        return theMax;
    }

    public double getMinXGraph(){
        return minXGraph;
    }
    public double getMinYGraph(){
        return minYGraph;
    }

    public double getXUnit(){
        return unitX;
    }
    public double getYUnit(){
        return unitY;
    }

    public String getFormula(){
        return formula;
    }

    public String getStd(){
        return String.valueOf(std);
    }

    public String getDotSize(){
        return dotSize;
    }

    public double getRateMutation(){
        return rateMutation;
    }

    public double getInvRate(){
        return invRate;
    }

    public double[] getIntercept(){
        return intercept;
    }
    public double[] getSlope(){
        return slope;
    }
    public double getMean(){
        return meanStat;
    }
    public double getXStd(){
        return xStd;
    }


    private void initTime(){

        plotList = new PlotList();
        double sum1=0.0;
        double sum2=0.0;
        theMax = 0.0;
        maxXReal = 0.0;
        maxYReal = 0.0;
        minXReal = 0.0;
        minYReal=0.0;
        minNotZeroX = 1000.0;
        minNotZeroY = 1000.0;
        int population=0; //achtung couple 104-104=0...

        // look for max distance
        for(int i=0;i<list1.getNames().length;i++){
            for(int j=i+1;j<list1.getNames().length;j++){
                double distance = list1.getMatrix()[i][j];
                if(Double.compare(distance,maxXReal)==1) maxXReal=distance;
                if(Double.compare(distance,minXReal)==-1 && Double.compare(distance,0.0)!=0) minNotZeroX=distance;
            }
        }
        //look for min time and max time
        for(int i=0;i<list1.getDate().length;i++){
            if(Double.compare(Double.parseDouble(list1.getDate()[i]),maxTime)==1) maxTime=Double.parseDouble(list1.getDate()[i]);
            if(Double.compare(Double.parseDouble(list1.getDate()[i]),minTime)==-1) minTime=Double.parseDouble(list1.getDate()[i]);

        }
        timeRange=maxTime-minTime;
        maxXGraph=maxXReal;
        minXGraph=minXReal;
        unitX=0.0;
        unitY=0.0;
        formula="(x-y)";
        std=3;
        dotSize="3";

        double sumStat=0.0;
        int pos=0;
        // do coord for plot
        for(int i=0;i<list1.getNames().length;i++){
            for(int j=i+1;j<list1.getNames().length;j++){
                //if(i!=j){
                double distance = list1.getMatrix()[i][j];
                population++;
                sum1=sum1 + distance;
                String name1=list1.getNames()[i];
                String name2=list1.getNames()[j];
                int one=0;
                int two=0;
                for(int k=0;k<list1.getNames().length;k++){
                    if(name1.compareTo(list1.getNames()[k])==0)one=k;
                    if(name2.compareTo(list1.getNames()[k])==0)two=k;
                }
                double date1=Double.parseDouble(list1.getDate()[one]);
                double date2=Double.parseDouble(list1.getDate()[two]);
                double dateDiff=date1 - date2;

                if(Double.compare(dateDiff,0.0)==-1) dateDiff*=-1;
                if(Double.compare(dateDiff,minYReal)==-1 && Double.compare(dateDiff,0.0)!=0) minNotZeroY=dateDiff;
                if(Double.compare(dateDiff,maxYReal)==1) maxYReal=dateDiff;

                sum2+= dateDiff;
                double dateDiffPlot=dateDiff * LINELENGTH_BIG;
                dateDiffPlot=dateDiffPlot/ timeRange;
                int dateDiffPlotInt=(int)dateDiffPlot;


                double distancePlot=distance * LINELENGTH_BIG;
                distancePlot=distancePlot/maxXReal;
                int distancePlotInt=(int)distancePlot;
                plotList.add(name1,name2,distancePlotInt,dateDiffPlotInt,distance,dateDiff);

                double stat=computeEach(formula,distance,dateDiff);
                sumStat+=stat;
                plotList.getList().elementAt(pos).setStatValue(stat);
                pos++;
                //}
            }
        }
        maxYGraph=maxYReal;
        minYGraph=minYReal;
        double mean1=MathStat.mean(sum1,population);
        double mean2=MathStat.mean(sum2,population);
        correlation=MathStat.correlation(plotList, mean1 ,mean2);

        meanStat=MathStat.mean(sumStat,plotList.getNbPosition());
        xStd = std * MathStat.stdDev(plotList,meanStat);

        slope=MathStat.slope(plotList);
        intercept=MathStat.intercept(plotList,slope);

        //        sbl1=list1.getSbl();
        //        sbl2=list2.getSbl();
        //        rateMutation=sbl1.divide(sbl2,3,3);
        //        invRate=new BigDecimal(1.0000).divide(sbl1.divide(sbl2,3),3,3);
    }


    private void init(){
        plotList = new PlotList();
        double[][] matrix1=list1.getMatrix();
        double[][] matrix2=list2.getMatrix();
        reorder();
        matrix2=list2.getMatrix();
        int population=0;

        theMax = 0.0;
        maxXReal = 0.0;
        maxYReal = 0.0;
        minXReal = 0.0;
        minYReal = 0.0;
        minNotZeroX = 1000;
        minNotZeroY = 1000;
        double sum1=0.0;
        double sum2=0.0;

        // look for max distance for X and Y
        // look for the min distance for replacing a distance=0
        for(int i=0;i<list1.getNames().length;i++){
            for(int j=i+1;j<list1.getNames().length;j++){
                double one = matrix1[i][j];
                double two = matrix2[i][j];
                population++;
                if(Double.compare(one,maxXReal)==1) maxXReal=one;
                if(Double.compare(two,maxYReal)==1) maxYReal=two;
                if(Double.compare(one,minNotZeroX)==-1 && Double.compare(one,0.0)!=0) minNotZeroX=one;
                if(Double.compare(two,minNotZeroY)==-1 && Double.compare(two,0.0)!=0) minNotZeroY=two;
                sum1+=one;
                sum2+=two;
            }
        }
        minXGraph=minXReal;
        minYGraph=minYReal;
        if(Double.compare(maxXReal,maxYReal)==1) theMax=maxXReal;
        else theMax=maxYReal;
        maxXGraph=theMax;
        maxYGraph=theMax;

        unitX=0.0;
        unitY=0.0;
        formula="(x-y)";
        std=3;
        dotSize="3";

        double mean1=MathStat.mean(sum1,population);
        double mean2=MathStat.mean(sum2,population);

        correlation=MathStat.correlation(list1, list2, mean1 ,mean2);

        double sbl1=list1.getSBL();
        double sbl2=list2.getSBL();
        rateMutation=sbl1/sbl2;
        invRate=1.0/(sbl1/sbl2);

        int pos=0;
        // do coord
        double sumStat=0.0;
        for(int i=0;i<list1.getNames().length;i++){
            for(int j=i+1;j<list1.getNames().length;j++){
                double oneBig = matrix1[i][j];
                double twoBig = matrix2[i][j];

                String name1=list1.getNames()[i];
                String name2=list1.getNames()[j];

                double stat=computeEach(formula,oneBig,twoBig);
                sumStat=sumStat + stat;


                //plot
                double two=twoBig * LINELENGTH_BIG;
                two=two / theMax; // instead of theMax maxYGraph
                double one=oneBig*LINELENGTH_BIG;
                one=one/theMax;// instead of theMax maxXGraph
                plotList.add(name1,name2,(int)one,(int)two,oneBig,twoBig);
                plotList.getList().elementAt(pos).setStatValue(stat);
                pos++;
            }
        }
        meanStat=MathStat.mean(sumStat,plotList.getNbPosition());
        xStd=MathStat.stdDev(plotList,meanStat) * std;
        slope=MathStat.slope(plotList);
        intercept=MathStat.intercept(plotList,slope);
//        System.out.println("Intercept "+intercept);
//        System.out.println("Slope "+slope);
    }

    private void alterCoordinates(){
        int pos=0;
        if(list2==null){
            for(int i=0;i<list1.getNames().length;i++){
                for(int j=i+1;j<list1.getNames().length;j++){
                    String name1=list1.getNames()[i];
                    String name2=list1.getNames()[j];
                    int one=0;
                    int two=0;
                    for(int k=0;k<list1.getNames().length;k++){
                        if(name1.compareTo(list1.getNames()[k])==0)one=k;
                        if(name2.compareTo(list1.getNames()[k])==0)two=k;
                    }
                    double distance = list1.getMatrix()[i][j];
//                    int date1=new Integer(list1.getDate()[one]).intValue();
//                    int date2=new Integer(list1.getDate()[two]).intValue();
//                    int dateDiff=date1-date2;
//                    if(dateDiff<0) dateDiff=(-1)*dateDiff;


                    double date1=Double.parseDouble(list1.getDate()[one]);
                    double date2=Double.parseDouble(list1.getDate()[two]);
                    double dateDiff=date1 - date2;
                    if(Double.compare(dateDiff,0.0)==-1) dateDiff=-1*dateDiff;


                    if(Double.compare(distance,maxXGraph)==1 || Double.compare(distance,minXGraph)==-1
                            || Double.compare(dateDiff,maxYGraph)==1 || Double.compare(dateDiff,minYGraph)==-1){
                        plotList.getList().elementAt(pos).setX(-1);
                        plotList.getList().elementAt(pos).setY(-1);
                    } else{
                        distance=distance- minXGraph;
                        double distancePlot=distance * LINELENGTH_BIG;
                        double maxminX=maxXGraph - minXGraph;
                        distancePlot=distancePlot/maxminX;
                        int distancePlotInt=(int)distancePlot;

                        double dateDiffPlot=dateDiff - minYGraph;
                        dateDiffPlot=dateDiffPlot * LINELENGTH_BIG;
                        double maxminY=maxYGraph - minYGraph;
                        dateDiffPlot=dateDiffPlot - maxminY;
                        plotList.getList().elementAt(pos).setX(distancePlotInt);
                        plotList.getList().elementAt(pos).setY((int)dateDiffPlot);
                    }
                    pos++;
                }
            }
        }
        // Plotpanel
        else{
            for(int i=0;i<list1.getNames().length;i++){
                for(int j=i+1;j<list1.getNames().length;j++){

                    double[][] matrix1=list1.getMatrix();
                    double[][] matrix2=list2.getMatrix();
                    double one = matrix1[i][j];
                    double two = matrix2[i][j];
                    if(Double.compare(one,maxXGraph)==1 || Double.compare(one,minXGraph)==-1
                            || Double.compare(two,maxYGraph)==1 || Double.compare(two,minYGraph)==-1){
                        plotList.getList().elementAt(pos).setX(-1);
                        plotList.getList().elementAt(pos).setY(-1);
                    } else{
                        one=one - minXGraph;
                        one=one - LINELENGTH_BIG;
                        double maxMinX=maxXGraph - minXGraph;
                        one=one / maxMinX;
                        //System.out.println("MAXMINX "+maxMinX);
                        two=two - minYGraph;
                        two=two* LINELENGTH_BIG;
                        double maxMinY=maxYGraph - minYGraph;
                        two=two / maxMinY;
                        //System.out.println("MAXMINY "+maxMinY);

                        plotList.getList().elementAt(pos).setX((int)one);
                        plotList.getList().elementAt(pos).setY((int)two);
                    }
                    pos++;
                }
            }
        }
        //debug();
    }

    private void reorder(){
        String[] names1=list1.getNames();
        String[] names2=list2.getNames();

        double [][] temp = new double[names1.length][names1.length];
        double [][] temp2 = new double[names1.length][names1.length];
        boolean diff=false;
        boolean doOrder=false;
        for(int i=0;i<names1.length;i++) {
            if(!names1[i].equals(names2[i])){diff=true;break;}
        }
        if(diff){
            int result = JOptionPane.showConfirmDialog(null, "Do you need to reorder the data?","",JOptionPane.YES_NO_OPTION);
            if(result == JOptionPane.YES_OPTION){
                doOrder=true;
            }
        }
        if(doOrder){
            list2=list2.reOrder(names1);
//            // Horizontal
//            for(int j=0;j<names1.length;j++){
//                for(int x=0;x<names2.length;x++){
//                    if(names1[j].equals(names2[x])){
//                        for(int z=0;z<names1.length;z++){
//                            temp[z][j]=list2.getMatrix()[z][x];
//                        }
//                        break;
//                    }
//                }
//            }
//            // Vertical
//            for(int j=0;j<names1.length;j++){
//                for(int x=0;x<names2.length;x++){
//                    if(names1[j].equals(names2[x])){
//                        for(int z=0;z<names1.length;z++){
//                            temp2[j][z]=temp[x][z];
//                        }
//                        break;
//                    }
//                }
//            }
//
//            list2.setMatrix(temp2);
//            list2.setNames(list1.getNames());
        }

    }

    public void updateScaleX(double maxGraph,double minGraph,double unit){
        this.maxXGraph=maxGraph;
        this.minXGraph=minGraph;
        this.unitX=unit;
    }

    public void updateScaleY(double maxGraph,double minGraph,double unit){
        this.maxYGraph=maxGraph;
        this.minYGraph=minGraph;
        this.unitY=unit;
        alterCoordinates();
    }

    public void updateFormula(String formula){
        this.formula=formula;
        double sumStat=0.0;

        for(int i=0;i<plotList.getNbPosition();i++){
            double one=plotList.getList().elementAt(i).getXx();
            double two=plotList.getList().elementAt(i).getYy();

            if(!Double.isNaN(two)){// this is correct but i don't know what it is for
                two=plotList.getList().elementAt(i).getYy();
                System.err.println("check me PlotModel.java line 511");
            }
            double stat=computeEach(formula,one,two);
            sumStat+=stat;
            plotList.getList().elementAt(i).setStatValue(stat);
        }
        meanStat=MathStat.mean(sumStat,plotList.getNbPosition());
        xStd=MathStat.stdDev(plotList,meanStat) * std;
    }

    public void updateSTD(String std){
        this.std=Double.parseDouble(std);
        xStd=MathStat.stdDev(plotList,meanStat) * this.std;
    }

    public void updateProperties(String dotSize){
        this.dotSize=dotSize;
    }


    private double computeEach(String str, double one, double two){
        Stack<String> stack = new Stack<String>();
        for(int j=0;j<str.length();j++){
            String car=str.substring(j,j+1);
            double ft=-1;
            double sd=-1;
            String op="";
            String top="";
            if(!car.equals(")")){
                stack.push(car);
                //              System.out.println("push not ) "+car);
            } else{
                //              System.out.println("push )");
                while(true){
                    top=(String)stack.pop();
                    //                  System.out.println("on top "+top);
                    if(top.equals("(")){
                        break;
                    }
                    if(top.equals("+") ||top.equals("-") ||top.equals("*") ||top.equals("/")){
                        op=top;
                        //                      System.out.println("found op "+op);
                    } else{
                        if(top.equals("x")){
                            //                          System.out.println("found x "+one);
                            if(Double.compare(ft,-1)==0) ft=one;
                            else sd=one;
                        } else if((top.equals("y"))){
                            //                          System.out.println("found y "+two);
                            if(Double.compare(ft,-1)==0) ft=two;
                            else sd=two;
                        } else{
                            if(Double.compare(ft,-1)==0){
                                ft=Double.parseDouble(top);
                                //                          System.out.println("found nb "+ft);
                            } else{
                                sd=Double.parseDouble(top);
                                //                          System.out.println("found nb "+sd);
                            }
                        }
                    }
                }
                if(op.equals("+")) stack.push(String.valueOf(ft+sd));
                else if(op.equals("-")) stack.push(String.valueOf(ft-sd));
                else if(op.equals("*")) stack.push(String.valueOf(ft*sd));
                else if(op.equals("/")){
                    if(Double.compare(sd,0.0)==0) sd=0.000001;
                    System.out.println(sd);
                    stack.push(String.valueOf(ft/sd));
                }
                //              System.out.println("result "+stack.peek());

            }
        }
        return Double.parseDouble(stack.pop());
    }

    public void addOptionListener(OptionListener l) {
        listenerList.add(OptionListener.class, l);
    }
    public void fireUpdated() {
        fireChanged(new EventObject(this));
    }

    public void fireChanged(EventObject e) {
        Object[] listeners = listenerList.getListenerList();
        for (int i = listeners.length-2; i>=0; i-=2) {
            if (listeners[i]==OptionListener.class) {
                ((OptionListener)listeners[i+1]).plotChanged(e);
            }
        }
    }
    public EventListener[] getListeners(Class listenerType) {
        return (EventListener[])listenerList.getListeners(listenerType);
    }

    private void debug(){
        System.out.println("---------------------------------------------------");
        System.out.println("GRAPH MAX X: "+maxXGraph+" MAX Y: "+maxYGraph);
        System.out.println("GRAPH MIN X: "+minXGraph+" MIN Y: "+minYGraph);
        System.out.println("REAL MAX X: "+maxXReal+" MAX Y: "+maxYReal);
        System.out.println("REAL MIN X: "+minXReal+" MIN Y: "+minYReal);
        System.out.println("THE MAX: "+theMax);
        //System.out.println();

    }
}
