/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 *
 * @author  formmat
 */

package Patristic;
   
public class PlotCoord {
    
    private String name1;
    private String name2;
    private int x; // X coordinate in the graph
    private int y; // Y coordinate in the graph
    private double xx;  // real X value (distance)
    private double yy;  // real Y value (distance)
    
    private double statValue=-1;
        
    //new
    public PlotCoord(String name1, String name2, int x, int y, double xx, double yy) {
        this.name1=name1;
        this.name2=name2;
        this.x=x;
        this.y=y;
        this.xx=xx;
        this.yy=yy;
    }
    
    
    public void setX(int x){
        this.x=x;
    }
    public void setY(int y){
        this.y=y;
    }
    
    public void setXx(double xx){
        this.xx=xx;
    }
    
    public void setYy(double yy){
        this.yy=yy;
    }
       
    // Return the name #1
    public String getName1(){
        return name1;
    }
    
    // Return the name #1
    public String getName2(){
        return name2;
    }
    
    // Return coord x for the graph
    public int getX(){
        return x;
    }
    
    // Return coord y for the graph
    public int getY(){
        return y;
    }
    
    // Return real coordinate x
    public double getXx(){
        return xx;
    }
    
    // Return real coordinate y
    public double getYy(){
        return yy;
    }
        
    // new
    public double getStatValue(){
        return statValue;
    }
    
    public void setStatValue(double val){
        statValue=val;
    }
    
}
