/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 *
 * @author  formmat
 */
package Patristic;
import Patristic.tree.Node;
import Patristic.tree.Tree;
import java.util.*;

public class PatristicComputer {
    //private static Matrix matrix;
//    static Object[] array;
//    static String[] names;
//    //static int nbTaxon;
//    static BigDecimal [][] matrixClade;
    static double sbl=0;
    

    public static Matrix compute(String nexus, String fileName){
        Matrix matrix=null;
        try {
            Tree tree = Utils.createTree(nexus);
            double [][] matrixClade=new double[tree.getTaxa().size()][tree.getTaxa().size()];
            long begin =System.currentTimeMillis();
            for(int i=0;i<tree.getTaxa().size();i++){
                for(int j=0;j<tree.getTaxa().size();j++){
                    matrixClade[i][j]=0;
                }
            }
            
            Vector<String> names=new Vector<String>();
            computeAux(tree.getRoot(),tree.getTaxa().size(),matrixClade,names);
            
            matrix = new Matrix(names.toArray(new String[names.size()]),fileName);
            matrix.setMatrix(matrixClade);
            long end =System.currentTimeMillis();
            long time=(end-begin);
            System.out.println("Processing time "+time);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return matrix;
    }
    
    private static void computeAux(Node node, int N, double [][] matrixClade, Vector names){
        if(node.getName() != null){
            names.add(node.getName());
            for(int i=0;i<N;i++){
                if(i!=names.size()-1){
                  matrixClade[names.size()-1][i] = matrixClade[names.size()-1][i]+node.getBrlens();
                  matrixClade[i][names.size()-1]=matrixClade[names.size()-1][i];
                }
            }
        }
        
        for(Iterator<Node> it=node.getChildren().iterator();it.hasNext();){
            computeAux(it.next(),N,matrixClade,names);
        }
        // POST
        if(node.getName() == null){
            Vector<Integer> children=new Vector<Integer>();
            getChildrenName(node,children,names);
            for(int i=0;i<N;i++){
                for(int j=i+1;j<N;j++){
                    if(children.contains(i) ^ children.contains(j)){
                        matrixClade[i][j]=matrixClade[i][j]+node.getBrlens();
                        matrixClade[j][i]=matrixClade[i][j];
                    }
                }
            }
        }
    }
    
    private static void getChildrenName(Node node, Vector<Integer> clusters, Vector names){
        if(node.getName()!= null){
            clusters.add(names.indexOf(node.getName()));
        }
        for(Iterator<Node> it=node.getChildren().iterator();it.hasNext();){
            getChildrenName(it.next(),clusters,names);
        }
    }
    
    
    
    public static Matrix getDistances(Object[] nexus, String[] names, String fileName){
        Matrix matrix = new Matrix(names,fileName);
        double [][] matrixClade=new double[names.length][names.length];
        long begin =System.currentTimeMillis();
        for(int i=0;i<names.length;i++){
            for(int j=0;j<names.length;j++){
                matrixClade[i][j]=0;
            }
        }
        Vector cladeList = new Vector();
        Vector cladeOpen = new Vector();
        int i=0;
        Vector stack=new Vector();
        String word="";
        while (i<nexus.length){
            word=nexus[i].toString();
            if(word.equalsIgnoreCase("(")) {
                cladeList.addElement(new Vector());
                cladeOpen.addElement("true");
            } else if(isNumber(word) && (nexus[i-1].toString().equals("(")|| nexus[i-1].toString().equals(","))){
                for(int j=0;j<cladeList.size();j++){
                    if(cladeOpen.elementAt(j).equals("true")) ((Vector)cladeList.elementAt(j)).add(word);
                }
            } else if(word.equals(":")) {} else if(word.equals(",")) {} else if(word.equals(")")) {} else if(isNumber(word) && nexus[i-1].toString().equals(")") ){} else if(isNumber(word) && isNumber(nexus[i-2].toString())&& nexus[i-3].toString().equals(")") ){
                //if(new BigDecimal(word).compareTo(new BigDecimal(0))==-1) System.out.println("NEGATIVE VALUE "+word);
                sbl=sbl+ Math.abs(Double.parseDouble(word));
                Vector clade=new Vector();
                for(int j=cladeOpen.size()-1;j>=0;j--){
                    if(cladeOpen.elementAt(j).equals("true")){
                        cladeOpen.setElementAt("false", j);
                        clade=(Vector)cladeList.elementAt(j);
                        break;
                    }
                }
                
                for(int z=0;z<names.length;z++){
                    for(int j=z+1;j<names.length;j++){
                        if(clade.contains((Object)String.valueOf(j+1)) && clade.contains((Object)String.valueOf(z+1))){} else if(clade.contains((Object)String.valueOf(j+1)) || clade.contains((Object)String.valueOf(z+1))){
                            matrixClade[j][z]=matrixClade[j][z] + Math.abs(Double.parseDouble(word));
                            matrixClade[z][j]=matrixClade[z][j] + Math.abs(Double.parseDouble(word));
                        }
                    }
                }
            } else if(isNumber(word) && isNumber(nexus[i-2].toString()) ){
                //if(new BigDecimal(word).compareTo(new BigDecimal(0))==-1) System.out.println("NEGATIVE VALUE "+word);
                sbl=sbl + Math.abs(Double.parseDouble(word));
                int taxon=Integer.parseInt(nexus[i-2].toString());
                for(int y=0;y<names.length;y++){
                    if(y!=taxon-1)
                        matrixClade[taxon-1][y]=matrixClade[taxon-1][y] + Math.abs(Double.parseDouble(word));
                }
                for(int x=0;x<names.length;x++){
                    if(x!=taxon-1)
                        matrixClade[x][taxon-1]=matrixClade[x][taxon-1] + Math.abs(Double.parseDouble(word));
                }
                
            } else if(isNumber(word) && nexus[i-2].toString().equals(")") ){
                //if(new BigDecimal(word).compareTo(new BigDecimal(0))==-1) System.out.println("NEGATIVE VALUE "+word);
                sbl=sbl + Math.abs(Double.parseDouble(word));
                Vector clade=new Vector();
                for(int j=cladeOpen.size()-1;j>=0;j--){
                    if(cladeOpen.elementAt(j).equals("true")){
                        cladeOpen.setElementAt("false", j);
                        clade=(Vector)cladeList.elementAt(j);
                        break;
                    }
                }
                
                for(int z=0;z<names.length;z++){
                    for(int j=z+1;j<names.length;j++){
                        if(clade.contains((Object)String.valueOf(j+1)) && clade.contains((Object)String.valueOf(z+1))){} else if(clade.contains((Object)String.valueOf(j+1)) || clade.contains((Object)String.valueOf(z+1))){
                            matrixClade[j][z]=matrixClade[j][z] + Math.abs(Double.parseDouble(word));
                            matrixClade[z][j]=matrixClade[z][j] + Math.abs(Double.parseDouble(word));
                        }
                    }
                }
            } else{}
            
            i++;
        }
        
        long end =System.currentTimeMillis();
        long time=(end-begin);
        System.out.println("Processing time "+time);
        //System.out.println("SBL "+sbl);
        //        for(int x=0;x<nbTaxon;x++){
        //            for(int j=0;j<nbTaxon;j++){
        //                System.out.print(matrixClade[x][j]+" | ");
        //            }
        //            System.out.println("\r\n");
        //        }
        matrix.setMatrix(matrixClade);
        matrix.setSbl(sbl);
        return matrix;
    }
    
    
    private static boolean isNumber(String nb){
        try{
            Double.parseDouble(nb);
        } catch (NumberFormatException err){return false;}
        return true;
    }
}
