/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author  Fourment Mathieu
 */
package Patristic;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import org.freehep.graphics2d.VectorGraphics;
import java.awt.print.*;
import java.text.DecimalFormat;

/**
 * Graphic representation of the results
 */
public class PlotDistPanel extends JPanel implements OptionListener,Printable{
    
    private PlotModel plotModel;
    private static final int XLEFT=100;
    private static int LINELENGTH=400;
    private static final int WIDTHLINE=1;
    int valueX;
    int valueY;
    String infoEvent;
    String infoEvent1;
    String infoEvent2;
    
    String ft="";
    String sd="";
    
    boolean isRegress=false;
    boolean isDiffRatioChecked=false;
    boolean isDiffChecked=false;
    boolean isCaption=false;
    boolean isStat=false;
    boolean swap=false;
    
    int dotSize=3;
    private MouseMotionHandler mouseMotionHandler=new MouseMotionHandler();
    
    public PlotDistPanel(PlotModel optionObject){
        this.plotModel=optionObject;
        infoEvent="";
        infoEvent1="";
        infoEvent2="";
        addMouseListener(new mouseHandler(this));
        this.addComponentListener(new ComponentListener(){
            public void componentResized(ComponentEvent e){
                                System.out.println("W "+e.getComponent().getWidth());
                                System.out.println("H "+e.getComponent().getHeight());
            }
            public void componentHidden(ComponentEvent e){}
            public void componentShown(ComponentEvent e){}
            public void componentMoved(ComponentEvent e){}
        });
    }
    
    
    public void highLight(String first, String second){
        ft=first;
        sd=second;
        repaint();
    }
    
    private void drawGraph(VectorGraphics vg){
        Dimension dim = getSize();
        Insets insets = getInsets();
        vg.setColor(Color.white);
        vg.fillRect(insets.left, insets.top,
                dim.width-insets.left-insets.right,
                dim.height-insets.top-insets.bottom);
        setBackground(Color.WHITE);
        vg.setColor(Color.black);
        
        vg.setFont(new Font("Courier New",Font.BOLD,12));
        //g.fillRect(XLEFT,XLEFT,WIDTHLINE,LINELENGTH);//y axis
        vg.drawLine(XLEFT,XLEFT,XLEFT,XLEFT+LINELENGTH);
        //g.fillRect(XLEFT,XLEFT+LINELENGTH+3,LINELENGTH,WIDTHLINE);//x axis
        vg.drawLine(XLEFT,XLEFT+LINELENGTH,XLEFT+LINELENGTH,XLEFT+LINELENGTH);
//        vg.drawString("Correlation r= "+optionObject.getCorrelation().toString(),10,10);
//        vg.drawString("Rate SBL #1/#2 (#2/#1)= "+optionObject.getRateMutation().toString()+" ("+optionObject.getInvRate().toString()+")",300,10);
//        vg.drawString("SBL #1= "+optionObject.getSBL1().toString(),300,50);
//        vg.drawString("SBL #2= "+optionObject.getSBL2().toString(),300,70);
        if(isCaption){
            vg.drawString("Tips: "+infoEvent,10,30);
            if(swap){
                vg.drawString(plotModel.getList1().getFileName()+": "+infoEvent2,10,50);
                vg.drawString(plotModel.getList2().getFileName()+": "+infoEvent1,10,70);
            } else{
                vg.drawString(plotModel.getList1().getFileName()+": "+infoEvent1,10,50);
                vg.drawString(plotModel.getList2().getFileName()+": "+infoEvent2,10,70);
            }
        }
        if(swap){
            vg.drawString(plotModel.getList1().getFileName(),XLEFT-3,XLEFT-4);
            vg.drawString(plotModel.getList2().getFileName(),XLEFT+LINELENGTH+4,XLEFT+LINELENGTH);
        } else{
            vg.drawString(plotModel.getList2().getFileName(),XLEFT-3,XLEFT-4);
            vg.drawString(plotModel.getList1().getFileName(),XLEFT+LINELENGTH+4,XLEFT+LINELENGTH);
        }
        
        double yAbscissa=0.0;
        double yAbscissaReal = plotModel.getMinYGraph();
        // should be getMaxYReal() not getTheMaxReal() !!!!!!!!!!!!!!!
        String fmt="0.##";
        DecimalFormat df = new DecimalFormat(fmt);
        
        if(Double.compare(plotModel.getYUnit(),0.0)!=0){
            while(true){
                yAbscissa=yAbscissa + plotModel.getYUnit();
                yAbscissaReal=yAbscissaReal + plotModel.getYUnit();
                if(Double.compare(yAbscissa,plotModel.getMaxYGraph() - plotModel.getMinYGraph())==0 || Double.compare(yAbscissa,plotModel.getMaxYGraph() - plotModel.getMinYGraph())==1)
                    break;
                double y=yAbscissa * LINELENGTH;
                //y=y.divide(optionObject.getMaxYGraph(),1);
                y=y / (plotModel.getMaxYGraph() - plotModel.getMinYGraph());
                vg.drawLine(XLEFT-3, XLEFT+LINELENGTH-y, XLEFT, XLEFT+LINELENGTH-y+(int)plotModel.getMinYGraph());
                vg.drawString(String.valueOf(yAbscissaReal),XLEFT-40,XLEFT+LINELENGTH-y+(int)plotModel.getMinYGraph());
            }
            vg.drawLine(XLEFT-3, XLEFT, XLEFT, XLEFT);
            vg.drawString(df.format(plotModel.getMaxYGraph()),XLEFT-40,XLEFT+3);
        } else{
            vg.drawString(df.format(plotModel.getTheMaxReal()),XLEFT-40,XLEFT+3);
            vg.drawString(df.format(plotModel.getTheMaxReal()/2),XLEFT-40,XLEFT+(LINELENGTH/2)+3);
            vg.drawLine(XLEFT-3, XLEFT, XLEFT, XLEFT);
            vg.drawLine(XLEFT-3, XLEFT+(LINELENGTH/2), XLEFT, XLEFT+(LINELENGTH/2));
        }
        
        double xAbscissa=0.0;
        double xAbscissaReal=plotModel.getMinXGraph();
        // should be getMaxXReal() not getTheMaxReal() !!!!!!!!!!!!!!!
        if(Double.compare(plotModel.getXUnit(),0.0)!=0){
            while(true){
                xAbscissa += plotModel.getXUnit();
                xAbscissaReal += plotModel.getXUnit();
                if(Double.compare(xAbscissa,plotModel.getMaxXGraph()-plotModel.getMinXGraph())==0 || Double.compare(xAbscissa,plotModel.getMaxXGraph() - plotModel.getMinXGraph())==1)
                    break;
                double y=xAbscissa*LINELENGTH;
                //y=y.divide(optionObject.getMaxXGraph(),1);
                y=y / (plotModel.getMaxXGraph() - plotModel.getMinXGraph());
                vg.drawLine(XLEFT+(int)y, XLEFT+LINELENGTH, XLEFT+y, XLEFT+LINELENGTH+5);
                vg.drawString(String.valueOf(xAbscissaReal),XLEFT+(int)y-20,XLEFT+LINELENGTH+15);
            }
            vg.drawString(df.format(plotModel.getMaxXGraph() - plotModel.getMinXGraph()),XLEFT+LINELENGTH-20,XLEFT+LINELENGTH+15);
            vg.drawLine(XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH+5);
        } else{
            vg.drawString(df.format(plotModel.getTheMaxReal() / 2), XLEFT+(LINELENGTH/2)-20, XLEFT+LINELENGTH+15);
            vg.drawString(df.format(plotModel.getTheMaxReal() / 2), XLEFT+LINELENGTH-20, XLEFT+LINELENGTH+15);
            vg.drawLine(XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH, XLEFT+LINELENGTH+5);
            vg.drawLine(XLEFT+(LINELENGTH/2), XLEFT+LINELENGTH, XLEFT+(LINELENGTH/2), XLEFT+LINELENGTH+5);
        }
        
        
        vg.drawString("0",XLEFT-5,XLEFT+LINELENGTH+15);
        drawDots(vg);
        
        if(isRegress){
            double length=LINELENGTH;
            vg.setColor(Color.red);
            if(swap){
                double one=plotModel.getIntercept()[1] - plotModel.getMinXGraph();
                one=one * length;
                double maxMinX=plotModel.getMaxXGraph() - plotModel.getMinXGraph();
                one=one/maxMinX;
                
                if(Double.compare(plotModel.getMaxXGraph(),plotModel.getTheMaxReal())==0 && Double.compare(plotModel.getMaxYGraph(),plotModel.getTheMaxReal())==0
                && Double.compare(plotModel.getMinXReal(),plotModel.getMinXGraph())==0 && Double.compare(plotModel.getMinYReal(),plotModel.getMinYGraph())==0)
                    vg.drawLine(XLEFT,XLEFT+LINELENGTH-(int)one, XLEFT+LINELENGTH, XLEFT+LINELENGTH-(plotModel.getSlope()[1]*LINELENGTH)+one);
            } else{
                double one=plotModel.getIntercept()[0] - plotModel.getMinXGraph();
                one*=length;
                double maxMinX=plotModel.getMaxXGraph() - plotModel.getMinXGraph();
                one=one/maxMinX;
                
                if(Double.compare(plotModel.getMaxXGraph(),plotModel.getTheMaxReal())==0 && Double.compare(plotModel.getMaxYGraph(),plotModel.getTheMaxReal())==0
                && Double.compare(plotModel.getMinXReal(),plotModel.getMinXGraph())==0 && Double.compare(plotModel.getMinYReal(),plotModel.getMinYGraph())==0 )
                    vg.drawLine(XLEFT,XLEFT+LINELENGTH-(int)one, XLEFT+LINELENGTH, XLEFT+LINELENGTH-(plotModel.getSlope()[0]*LINELENGTH)+(int)one);
            }
        }
        //debug();
    }
    
    public void paintComponent(Graphics g) {
        if (g == null) return;
        dotSize=Integer.valueOf(plotModel.getDotSize()).intValue();
        VectorGraphics vg = VectorGraphics.create(g);
        drawGraph(vg);
    }
    
    // Draw one line: match line or mismatch line with their positions
    private void drawDots(VectorGraphics vg){
        int twoI=0;
        int oneI=0;
        int highLightx=-1;
        int highLighty=-1;
        PlotList plotList=plotModel.getPlotList();
        for(int i=0;i<plotList.getNbPosition();i++){
            if(swap){
                twoI=((PlotCoord)plotList.getList().elementAt(i)).getX();
                oneI=((PlotCoord)plotList.getList().elementAt(i)).getY();
            } else{
                oneI=((PlotCoord)plotList.getList().elementAt(i)).getX();
                twoI=((PlotCoord)plotList.getList().elementAt(i)).getY();
            }
            if(oneI==-1 || twoI==-1) continue;
            //            if(((PlotCoord)plotList.getList().elementAt(i)).getXx().compareTo(optionObject.getMaxXGraph().subtract(optionObject.getMinXGraph()))==1
            //            || ((PlotCoord)plotList.getList().elementAt(i)).getYy().compareTo(optionObject.getMaxYGraph().subtract(optionObject.getMinYGraph()))==1) continue;
            //
            //            if(((PlotCoord)plotList.getList().elementAt(i)).getXx().compareTo(optionObject.getMinXGraph())==-1
            //            || ((PlotCoord)plotList.getList().elementAt(i)).getYy().compareTo(optionObject.getMinYGraph())==-1) continue;
            
            if( (((PlotCoord)plotList.getList().elementAt(i)).getName1().compareTo(ft)==0 && ((PlotCoord)plotList.getList().elementAt(i)).getName2().compareTo(sd)==0) || ((PlotCoord)plotList.getList().elementAt(i)).getName1().compareTo(sd)==0 && ((PlotCoord)plotList.getList().elementAt(i)).getName2().compareTo(ft)==0){
                highLightx=oneI;
                highLighty=twoI;
                continue;
            }
//            else if(((PlotCoord)plotList.getList().elementAt(i)).isDiff() && isDiffChecked && !isDiffRatioChecked){
//                vg.setColor(Color.BLUE);
//                vg.fillRect(XLEFT+oneI,XLEFT+LINELENGTH-twoI,dotSize,dotSize);
//                continue;
//            }
            else if(isStat){
                boolean bool=false;
                double xstd=plotModel.getXStd();
                double mean=plotModel.getMean();
                double plusStd=mean+xstd;
                double minusStd=mean-xstd;
                double value=((PlotCoord)plotList.getList().elementAt(i)).getStatValue();
                
                if(Double.compare(value,plusStd)==1 || Double.compare(value,minusStd)==-1){
                    vg.setColor(Color.GREEN);
                    vg.fillRect(XLEFT+oneI,XLEFT+LINELENGTH-twoI,dotSize,dotSize);
                    bool=true;
                }
                if(bool)continue;
            }
//            else if(((PlotCoord)plotList.getList().elementAt(i)).isRatio() && isDiffRatioChecked &&!isDiffChecked ){
//                vg.setColor(Color.YELLOW);
//                vg.fillRect(XLEFT+oneI,XLEFT+LINELENGTH-twoI,dotSize,dotSize);
//                continue;
//            }
            vg.setColor(Color.BLACK);
            vg.fillRect(XLEFT+oneI,XLEFT+LINELENGTH-twoI,dotSize,dotSize);
        }
        if(highLightx>=0 ){
            vg.setColor(Color.RED);
            vg.fillRect(XLEFT+highLightx,XLEFT+LINELENGTH-highLighty,dotSize,dotSize);
        }
        
    }
    
    public void setRegress(boolean isRegress){
        this.isRegress=isRegress;
        repaint();
    }
    
    public void setDiffRatio(boolean isDiffRatio){
        this.isDiffRatioChecked=isDiffRatio;
        repaint();
    }
    public void setDiff(boolean isDiff){
        this.isDiffChecked=isDiff;
        repaint();
    }
    
    public void setCaption(boolean isCaption){
        this.isCaption=isCaption;
        if(isCaption) addMouseMotionListener(mouseMotionHandler);
        else this.removeMouseMotionListener(mouseMotionHandler);
        repaint();
    }
    
    public void swap(){
        if(swap)swap=false;
        else swap=true;
        repaint();
    }
    
    public void setStat(boolean stat){
        isStat=stat;
        repaint();
    }
    
    public void plotChanged(EventObject e) {
        repaint();
    }
    
    public int print(Graphics g, PageFormat pageFormat, int pi) throws PrinterException {
        if (pi >= 1) {
            return Printable.NO_SUCH_PAGE;
        }
        VectorGraphics vg = VectorGraphics.create(g);
        drawGraph(vg);
        return Printable.PAGE_EXISTS;
    }
    
    /**
     * Class used for the mouseMoved method, which is used for getting the coordinates
     * of the mouse and therefore to get information of a match or a mismatch
     */
    class MouseMotionHandler extends MouseMotionAdapter {
        public void mouseMoved(MouseEvent e) {
            PlotList plotList=plotModel.getPlotList();
            valueX=e.getX();
            valueY=e.getY();
            int oneI;
            int twoI;
            for(int i=0;i<plotList.getNbPosition();i++){
                if(swap){
                    oneI=((PlotCoord)plotList.getList().elementAt(i)).getY()+XLEFT;
                    twoI=XLEFT+LINELENGTH-((PlotCoord)plotList.getList().elementAt(i)).getX();
                } else{
                    oneI=((PlotCoord)plotList.getList().elementAt(i)).getX()+XLEFT;
                    twoI=XLEFT+LINELENGTH-((PlotCoord)plotList.getList().elementAt(i)).getY();
                }
                
                if ((valueX > oneI && valueX<=oneI+dotSize) && (valueY<=twoI+dotSize && valueY > twoI) ) {
                    infoEvent=((PlotCoord)plotList.getList().elementAt(i)).getName1()+" & "+((PlotCoord)plotList.getList().elementAt(i)).getName2();
                    infoEvent1=String.valueOf(((PlotCoord)plotList.getList().elementAt(i)).getXx());
                    infoEvent2=String.valueOf(((PlotCoord)plotList.getList().elementAt(i)).getYy());
                    repaint();
                    break;
                } else{
                    infoEvent="";
                    infoEvent1="";
                    infoEvent2="";
                }
            }
        }
    }
    
    private class mouseHandler extends java.awt.event.MouseAdapter {
        PlotDistPanel panel;
        mouseHandler(PlotDistPanel panel){
            this.panel=panel;
        }
        public void mouseClicked(MouseEvent e) {
            panel.swap();
            //        System.out.println("H: "+panel.getHeight());
            //        System.out.println("W: "+panel.getWidth());
        }
    }
    private void debug(){
        System.out.println("----------------------------");
        System.out.println("Max X graph: "+plotModel.getMaxXGraph()+" Min X graph: "+plotModel.getMinXGraph());
        System.out.println("Max Y graph: "+plotModel.getMaxYGraph()+" Min Y graph: "+plotModel.getMinYGraph());
        System.out.println("Max X real: "+plotModel.getMaxXReal()+" Min X real: "+plotModel.getMinXReal());
        System.out.println("Max Y real: "+plotModel.getMaxYReal()+" Min Y real: "+plotModel.getMinYReal());
        System.out.println("The max real: "+plotModel.getTheMaxReal());
    }
    
}
