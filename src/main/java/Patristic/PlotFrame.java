/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 *
 * @author  Fourment Mathieu
 */
package Patristic;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.EventObject;
import javax.swing.*;

import javax.swing.JTable.*;
import javax.swing.table.*;
//import org.freehep.graphicsio.ps.*;
import org.freehep.util.export.ExportDialog;

/**
 * GUI used for the graphic display of the results
 */
public class PlotFrame extends JFrame implements OptionListener,ItemListener{
    public JSplitPane splitPane,splitPane2;
    private DefaultTableModel model=new DefaultTableModel();
    TableSorter sorter = new TableSorter(model);
    private JTable table=new JTable(sorter);

    private PlotDistPanel panel;
    private PlotTimePanel panelTime;
    private JScrollPane scrollPanel;
    private JScrollPane scrollTable= new JScrollPane(table);

    private JMenuBar bar = new JMenuBar();
    private JMenu option = new JMenu("Option");
    private JCheckBoxMenuItem stat = new JCheckBoxMenuItem("Stats");
    private JMenuItem optionStat = new JMenuItem("Options");
    private JMenuItem print = new JMenuItem("Print");
    private JCheckBoxMenuItem regress=new JCheckBoxMenuItem("Regression");
    //private JCheckBoxMenuItem diffRatio=new JCheckBoxMenuItem("Diff*Ratio");
    //private JCheckBoxMenuItem diff=new JCheckBoxMenuItem("Diff");
    private JCheckBoxMenuItem caption=new JCheckBoxMenuItem("Caption");
    private JMenuItem exportGraphic=new JMenuItem("Export Graphic");
    private JMenuItem exportMatrix=new JMenuItem("Export Matrix");
    private Option optionFrame;


    private boolean isDiffRatio=false;
    private boolean isDiff=false;
    private boolean isCaption=false;

    PlotList list=null;
    private PlotModel plotObject;
    int h;
    int w;
    String[] columnIdentifiers=new String[5];
    boolean plotTime=false;

    JTabbedPane tabbedPane = new JTabbedPane();
    JTextArea info=new JTextArea();

    public PlotFrame(Matrix list1){
        plotTime=true;
        sorter.addMouseListenerToHeaderInTable(table);
        table.addMouseListener(new MyMouseListener());
        Dimension tailleEcran = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        h = (int)tailleEcran.getHeight();
        w = (int)tailleEcran.getWidth();
        setSize(w,h-30);
        //this.setResizable(false);
        setTitle("Time against distance: "+list1.getFileName());

        plotObject=new PlotModel(list1);
        panelTime = new PlotTimePanel(plotObject);
        plotObject.addOptionListener(panelTime);
        plotObject.addOptionListener(this);
        list=plotObject.getPlotList();
        //PlotList listCoord = panel.listCoord;
        //String[] columnIdentifiers = {"Name 1","Name 2",plotObject.getList2().getFileName(),plotObject.getList1().getFileName(),plotObject.getFormula()};
        columnIdentifiers[0] = "Name 1";
        columnIdentifiers[1] ="Name 2";
        columnIdentifiers[2] ="Distance";
        columnIdentifiers[3] ="Data difference";
        columnIdentifiers[4] =plotObject.getFormula();

        model.setColumnIdentifiers(columnIdentifiers);
        for(int i=0;i<list.getNbPosition();i++){
            String oneI=String.valueOf(((PlotCoord)list.getList().elementAt(i)).getXx());
            String twoI=String.valueOf(((PlotCoord)list.getList().elementAt(i)).getYy());
            String stat=String.valueOf(((PlotCoord)list.getList().elementAt(i)).getStatValue());
            Object[] data = {((PlotCoord)list.getList().elementAt(i)).getName1(),((PlotCoord)list.getList().elementAt(i)).getName2(),oneI,twoI,stat};
            model.insertRow(model.getRowCount(),data);
        }

        scrollPanel= new JScrollPane(panelTime);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        layoutMiddle();
    }

    /** Creates a new instance of Diagram */
    public PlotFrame(Matrix list1, Matrix list2){

        sorter.addMouseListenerToHeaderInTable(table);
        table.addMouseListener(new MyMouseListener());
        Dimension tailleEcran = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        h = (int)tailleEcran.getHeight();
        w = (int)tailleEcran.getWidth();
        setSize(w,h-30);

        setTitle("Plot "+list1.getFileName()+" against "+list2.getFileName());

        plotObject=new PlotModel(list1,list2);
        try{
            panel = new PlotDistPanel(plotObject);
        } catch(ClassFormatError q){}
        plotObject.addOptionListener(panel);
        plotObject.addOptionListener(this);
        list=plotObject.getPlotList();
        //PlotList listCoord = panel.listCoord;
        //String[] columnIdentifiers = {"Name 1","Name 2",plotObject.getList2().getFileName(),plotObject.getList1().getFileName(),plotObject.getFormula()};
        columnIdentifiers[0] = "Name 1";
        columnIdentifiers[1] ="Name 2";
        columnIdentifiers[2] =plotObject.getList2().getFileName();
        columnIdentifiers[3] =plotObject.getList1().getFileName();
        columnIdentifiers[4] =plotObject.getFormula();
        model.setColumnIdentifiers(columnIdentifiers);
        for(int i=0;i<list.getNbPosition();i++){
            String twoI=String.valueOf(((PlotCoord)list.getList().elementAt(i)).getXx());
            String oneI=String.valueOf(((PlotCoord)list.getList().elementAt(i)).getYy());
            String stat=String.valueOf(((PlotCoord)list.getList().elementAt(i)).getStatValue());
            Object[] data = {((PlotCoord)list.getList().elementAt(i)).getName1(),((PlotCoord)list.getList().elementAt(i)).getName2(),oneI,twoI,stat};
            model.insertRow(model.getRowCount(),data);
        }

        scrollPanel= new JScrollPane(panel);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        layoutMiddle();
    }

    public void clickStat(MouseEvent e){
        optionFrame = new Option(plotObject);
        optionFrame.setVisible(true);
    }

    // Create the textareas
    private void layoutMiddle() {
        Container pane = this.getContentPane();
        //JPanel top = new JPanel();
        //top.setLayout(new BorderLayout());
        bar.add(option);
        option.add(stat);
        stat.addItemListener(this);
        option.add(exportMatrix);
        option.add(exportGraphic);
        option.add(optionStat);
        option.add(print);
        optionStat.addMouseListener(new MyMouseAdapter(this));


        option.add(regress);
        regress.addItemListener(this);

        //        option.add(diffRatio);
        //        diffRatio.addActionListener(new ActionListener(){
        //            public void actionPerformed(ActionEvent e) {
        //                if(isDiffRatio){
        //                    isDiffRatio=false;
        //                    panel.setDiffRatio(false);
        //                    diffRatio.setSelected(false);
        //                }
        //                else{
        //                    isDiffRatio=true;
        //                    panel.setDiffRatio(true);
        //                    diffRatio.setSelected(true);
        //                }
        //            }
        //        });
        //        option.add(diff);
        //        diff.addActionListener(new ActionListener(){
        //            public void actionPerformed(ActionEvent e) {
        //                if(isDiff){
        //                    isDiff=false;
        //                    panel.setDiff(false);
        //                    diff.setSelected(false);
        //                }
        //                else{
        //                    isDiff=true;
        //                    panel.setDiff(true);
        //                    diff.setSelected(true);
        //                }
        //            }
        //        });
        option.add(caption);
        caption.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if(isCaption){
                    isCaption=false;
                    panel.setCaption(false);
                    caption.setSelected(false);
                } else{
                    isCaption=true;
                    panel.setCaption(true);
                    caption.setSelected(true);
                }
            }
        });
        exportGraphic.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                ExportDialog export = new ExportDialog();
                if(panel!=null) export.showExportDialog(panel, "Save as ...", panel, "export");
                else export.showExportDialog(panel, "Save as ...", panelTime, "export");
            }
        });
        
        exportMatrix.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                JFileChooser filechooser = new JFileChooser();
                //filechooser.setCurrentDirectory(path);
                filechooser.addChoosableFileFilter(new FiltreExtension("csv", "CSV file"));
                int returnVal = filechooser.showSaveDialog(panel);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                  String filename = filechooser.getSelectedFile().toString();
                  BufferedWriter writer = null;
                  try {
                    writer = new BufferedWriter(new FileWriter(filename));

                    writer.write("taxon 1,taxon 2,"+plotObject.getList1().getFileName()+","+plotObject.getList2().getFileName()+","+"stat");
                    writer.newLine();
                    for(int i=0;i<list.getNbPosition();i++){
                        String dist1 = String.valueOf(list.getList().elementAt(i).getXx());
                        String dist2 = String.valueOf(list.getList().elementAt(i).getYy());
                        String stat  = String.valueOf(list.getList().elementAt(i).getStatValue());
                        writer.write(list.getList().elementAt(i).getName1()+","+list.getList().elementAt(i).getName2() + "," + dist1 + "," + dist2 + "," + stat);
                        writer.newLine();
                    }

                  } catch (IOException ex) {
                    ex.printStackTrace();
                  } finally {
                    try {
                      if (writer != null) {
                        writer.flush();
                        writer.close();
                      }

                    } catch (IOException exx) {
                      exx.printStackTrace();
                    }
                  }
                }

            }
        });
        
        print.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();
                printJob.setPrintable(panel);
                if (printJob.printDialog()) {
                    try {
                        printJob.print();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        pane.add(bar, BorderLayout.NORTH);

        JTextArea stat=new JTextArea();
        pane.add(stat,BorderLayout.SOUTH);
        //table.setRowSelectionInterval(100,100);
        tabbedPane.add("Distances",scrollTable);
        tabbedPane.add("Info",info);
        info.append("Correlation r="+plotObject.getCorrelation()+"\n");
        info.append("Slope ="+plotObject.getSlope()[0]+"("+plotObject.getSlope()[1]+")\n");
        info.append("Intercept ="+plotObject.getIntercept()[0]+"("+plotObject.getIntercept()[1]+")\n");
        info.append("SBL #1= "+String.valueOf(plotObject.getList1().getSBL())+"\n");
        if(!plotTime){
            info.append("SBL #2= "+String.valueOf(plotObject.getList2().getSBL())+"\n");
            info.append("SBL ratio #1/#2 (#2/#1)= "+String.valueOf(plotObject.getRateMutation())+" ("+String.valueOf(plotObject.getInvRate())+")\n");
        } else{

        }
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scrollPanel, tabbedPane);
        splitPane.setDividerLocation(600);
        splitPane.setFont(new Font("Courier New",Font.PLAIN,12));
        splitPane.setOneTouchExpandable(true);
        Dimension minimumSize = new Dimension(100, 50);
        scrollPanel.setMinimumSize(minimumSize);
        scrollTable.setMinimumSize(minimumSize);
        pane.add(splitPane,"Center");
    }

    public void itemStateChanged(ItemEvent e){
        Object obj=e.getItemSelectable();
        JCheckBoxMenuItem check=(JCheckBoxMenuItem)obj;
        if(obj == stat){
            if(!check.isSelected()) panel.setStat(false);
            else panel.setStat(true);
        } else if(obj == regress){
            if(!check.isSelected()) panel.setRegress(false);
            else panel.setRegress(true);
        }
    }

    class MyMouseListener implements MouseListener {
        public void mouseClicked(MouseEvent e) {
            JTable target = (JTable)e.getSource();
            int row = target.getSelectedRow();
            String first=(String)table.getValueAt(row, 0);
            String second=(String)table.getValueAt(row, 1);
            if(!plotTime)
                panel.highLight(first,second);
            else panelTime.highLight(first,second);

        }
        public void mouseReleased(MouseEvent e){

        }
        public void mousePressed(MouseEvent e){

        }
        public void mouseExited(MouseEvent e){

        }
        public void mouseEntered(MouseEvent e){

        }
    }

    public PlotList getPlotList(){
        return list;
    }

    //        public void setPlotList(PlotList list){
    //            this.list=list;
    //            panel.setList(this.list);
    //            for(int i=0;i<list.getNbPosition();i++){
    //                model.setValueAt(((PlotCoord)list.getList().elementAt(i)).getStatValue(), i, 4);
    //                model.fireTableCellUpdated(i,4);
    //            }
    //        }

    public void plotChanged(EventObject e) {
        //String[] columnIdentifiers = {"Name 1","Name 2",plotObject.getList2().getFileName(),plotObject.getList1().getFileName(),plotObject.getFormula()};
        columnIdentifiers[4]=plotObject.getFormula();
        model.setColumnIdentifiers(columnIdentifiers);
        for(int i=0;i<list.getNbPosition();i++){
            model.setValueAt(((PlotCoord)list.getList().elementAt(i)).getStatValue(), i, 4);
            model.fireTableCellUpdated(i,4);
        }
    }

    private class MyMouseAdapter extends java.awt.event.MouseAdapter {
        PlotFrame adaptee;

        MyMouseAdapter(PlotFrame adaptee) {
            this.adaptee = adaptee;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            adaptee.clickStat(e);
        }
    }



}


