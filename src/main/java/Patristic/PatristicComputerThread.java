/*
 *   Patristic is a Java program that uses as input different tree files
 *   and computes their patristic distances.
 *   Copyright (C) 2005 M Fourment
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Patristic;

import java.util.Vector;

/**
 *
 * @author fourmat
 */
public class PatristicComputerThread {

  Matrix matrix = null;
  private int lengthOfTask;
  Object[] nexus = null;
  String[] names = null;
  String fileName = null;
  double[][] matrixClade = null;
  boolean done = false;
  int current = 0;
  Patristic patristic;

  /** Creates a new instance of PatristicComputerThread */
  public PatristicComputerThread(Patristic patristic, Object[] nexus, String[] names, String fileName) {
    this.patristic = patristic;
    this.nexus = nexus;
    this.names = names;
    this.fileName = fileName;
    matrix = new Matrix(names, fileName);
    matrixClade = new double[names.length][names.length];
    lengthOfTask = names.length;//((names.length*names.length)-names.length)/2;
    for (int i = 0; i < names.length; i++) {
      for (int j = 0; j < names.length; j++) {
        matrixClade[i][j] = 0;
      }
    }
  }

  public void go() {
    final SwingWorker worker = new SwingWorker() {

      public Object construct() {
        current = 0;
        done = false;
        return new Task();
      }
    };
    worker.start();
  }

  public boolean isDone() {
    return done;
  }

  public int getCurrent() {
    return current;
  }

  public int getLengthOfTask() {
    return lengthOfTask;
  }

  public void finish() {
    PatristicTableModel model = (PatristicTableModel) patristic.getTable().getModel();
    model.addRow(matrix);
  }

  class Task {

    Task() {
      long begin = System.currentTimeMillis();
      Vector cladeList = new Vector();
      Vector cladeOpen = new Vector();
      double sbl = 0;
      int i = 0;
      Vector stack = new Vector();
      String word = "";
      while (i < nexus.length) {
        word = nexus[i].toString();
        if (word.equalsIgnoreCase("(")) {
          cladeList.addElement(new Vector());
          cladeOpen.addElement("true");
        } // Taxon name after a ( or ,
        else if (isNumber(word) && (nexus[i - 1].toString().equals("(") || nexus[i - 1].toString().equals(","))) {
          for (int j = 0; j < cladeList.size(); j++) {
            if (cladeOpen.elementAt(j).equals("true")) {
              ((Vector) cladeList.elementAt(j)).add(word);
            }
          }
        } else if (word.equals(":")) {
        } else if (word.equals(",")) {
        } else if (word.equals(")")) {
        } else if (isNumber(word) && nexus[i - 1].toString().equals(")")) {
        } // Don't how
        else if (isNumber(word) && isNumber(nexus[i - 2].toString()) && nexus[i - 3].toString().equals(")")) {
          //if(new BigDecimal(word).compareTo(new BigDecimal(0))==-1) System.out.println("NEGATIVE VALUE "+word);
          sbl = sbl+Double.parseDouble(word);
          Vector clade = new Vector();
          for (int j = cladeOpen.size() - 1; j >= 0; j--) {
            if (cladeOpen.elementAt(j).equals("true")) {
              cladeOpen.setElementAt("false", j);
              clade = (Vector) cladeList.elementAt(j);
              break;
            }
          }

          for (int z = 0; z < names.length; z++) {
            for (int j = z + 1; j < names.length; j++) {
              if (clade.contains((Object) String.valueOf(j + 1)) && clade.contains((Object) String.valueOf(z + 1))) {
              } else if (clade.contains((Object) String.valueOf(j + 1)) || clade.contains((Object) String.valueOf(z + 1))) {
                matrixClade[j][z] = matrixClade[j][z] + Math.abs(Double.parseDouble(word));
                matrixClade[z][j] = matrixClade[z][j] + Math.abs(Double.parseDouble(word));
              //current++;
              }
            }
          }
        } // Distance after TaxonName:
        else if (isNumber(word) && isNumber(nexus[i - 2].toString())) {
          //if(new BigDecimal(word).compareTo(new BigDecimal(0))==-1) System.out.println("NEGATIVE VALUE "+word);
          sbl += Math.abs(Double.parseDouble(word));
          int taxon = Integer.parseInt(nexus[i - 2].toString());
          for (int y = 0; y < names.length; y++) {
            if (y != taxon - 1) {
              matrixClade[taxon - 1][y] = matrixClade[taxon - 1][y] + Math.abs(Double.parseDouble(word));
            }
          }
          for (int x = 0; x < names.length; x++) {
            if (x != taxon - 1) {
              matrixClade[x][taxon - 1] = matrixClade[x][taxon - 1] + Math.abs(Double.parseDouble(word));
            }
          }
          current++;

        } // Clade distance ex: }:0.01
        else if (isNumber(word) && nexus[i - 2].toString().equals(")")) {

          //if(new BigDecimal(word).compareTo(new BigDecimal(0))==-1) System.out.println("NEGATIVE VALUE "+word);
          sbl = sbl+ Math.abs(Double.parseDouble(word));
          Vector clade = new Vector();
          for (int j = cladeOpen.size() - 1; j >= 0; j--) {
            if (cladeOpen.elementAt(j).equals("true")) {
              cladeOpen.setElementAt("false", j);
              clade = (Vector) cladeList.elementAt(j);
              break;
            }
          }

          for (int z = 0; z < names.length; z++) {
            for (int j = z + 1; j < names.length; j++) {
              if (clade.contains((Object) String.valueOf(j + 1)) && clade.contains((Object) String.valueOf(z + 1))) {
              } else if (clade.contains((Object) String.valueOf(j + 1)) || clade.contains((Object) String.valueOf(z + 1))) {
                matrixClade[j][z] = matrixClade[j][z]+ Math.abs(Double.parseDouble(word));
                matrixClade[z][j] = matrixClade[z][j]+ Math.abs(Double.parseDouble(word));
              }
            }
          }
        } else {
        }

        i++;
      }

      matrix.setMatrix(matrixClade);
      matrix.setSbl(sbl);
      done = true;

      long end = System.currentTimeMillis();
      long time = (end - begin);
      System.out.println("Processing time " + time);
    }

    private boolean isNumber(String nb) {
      try {
        Double.parseDouble(nb);
      } catch (NumberFormatException err) {
        return false;
      }
      return true;
    }
  }
}
